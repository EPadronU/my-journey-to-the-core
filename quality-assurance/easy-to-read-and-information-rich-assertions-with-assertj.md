# Easy-to-read and information-rich assertions with AssertJ

Assertions are the most used mechanism for making sure that a series of actions
performed on our system under test (or SUT for short) makes it behave just as
we expect, producing a predictable outcome. Sadly, assertions are not given the
thought they deserve 'till they fail, which is when we absolutely need to
understand not only the original intention that spawned the creation of the
failed check, but more importantly to grasp what failed and how. That's why
it's so critical to write good assertions that provide us clues when something
goes south, not for when we are creating the tests and they pass as we expect,
but for when we need to understand their nature; either for code-reviewing
other's tests, for trying to understand tests that has been written even as
recently as a couple of weeks ago or, even more importantly, for figuring out
why an assertion failed.

With a fluent-assertions library like AssertJ and a bit of discipline is easy
to create very readable and information-rich assertions. And in order to show
you the advantages integrating a library like AssertJ brings to the table, we
are going to compare the JUnit5's way of writing assertions vs AssertJ's
flavor. Additionally, we'll go through the messages both libraries generate by
default when something fails, as well as the mechanisms both libraries provide
for customizing such massages. Last, but not least, we'll cover how each of
these libraries manage soft-assertions.

First, lets do a basic verification on a String as follows:

```java
  public static final String EXPECTED = "Green Ranger";

  public static final String ACTUAL = "White Ranger";

  @Test
  public void junit5(){
    assertEquals(EXPECTED, ACTUAL);
  }
  /* org.opentest4j.AssertionFailedError:
   * Expected :Green Ranger
   * Actual   :White Ranger */

  @Test
  public void assertj() {
    assertThat(ACTUAL).isEqualTo(EXPECTED);
  }
  /* org.opentest4j.AssertionFailedError:
   * expected: "Green Ranger"
   * but was: "White Ranger"
   * Expected :"Green Ranger"
   * Actual   :"White Ranger" */
```

At first glance, the difference between the two libraries is negligible.
Perhaps, you're thinking that AssertJ's version is more verbose than JUnit5's
and objectively you're right, but there's a great advantage AssertJ introduces
with the help of your IDE of preference: code completion. So, you only have to
invoke the `assertThat` method and your IDE will suggest every assertion that
makes sense for your _actual value_'s datatype; plus you get many more
specialized assertion methods than the basic ones Junit5 provides. And on that
last note, let's contrast how we can perform more specific checks:

```java
  public static final String EXPECTED = "Green Ranger";

  public static final String ACTUAL = "White Ranger";

  @Test
  public void junit5() {
    assertNotNull(ACTUAL);
    assertFalse(ACTUAL.isBlank());
    assertTrue(ACTUAL.startsWith("G"));
    assertTrue(ACTUAL.endsWith("Power Ranger"));
  }
  /* org.opentest4j.AssertionFailedError:
   * Expected :true
   * Actual   :false */

  @Test
  void assertj() {
    assertThat(ACTUAL)
     .isNotBlank()
     .startsWith("G")
     .endsWith("Power Ranger");
  }
  /* java.lang.AssertionError:
   * Expecting actual:
   *   "White Ranger"
   * to start with:
   *   "G" */
```

Above we can appreciate the first set of critical differences: JUnit5 offers a
very basic collection of assertion methods, alongside a shallow failure
message. In contrast, AssertJ mades it easy to write assertions in a fluid way
that's close to plain-old English, plus the failure message tells us exactly
why the assertion failed. No guessing games involved.

Now, wait a minute. Our `ACTUAL` value doesn't end with the substring `Power
Ranger`. Why did both of these libraries miss to report such a thing? That's
because regular assertions (also called hard-assertions) are designed to throw
an assertion error as soon as the condition their checking doesn't hold,
interrupting the test's execution thus avoiding the evaluation of the
assertions that follows the one that failed. In order to get around this
behaviour, we can use soft-assertions:

```java
  public static final String EXPECTED = "Green Ranger";

  public static final String ACTUAL = "White Ranger";

  @Test
  public void junit5() {
    assertAll(
    () -> assertNotNull(ACTUAL),
    () -> assertFalse(ACTUAL.isBlank()),
    () -> assertTrue(ACTUAL.startsWith("G")),
    () -> assertTrue(ACTUAL.endsWith("Power Ranger")));
  }
  /* expected: <true> but was: <false>
   * Comparison Failure:
   * Expected :true
   * Actual   :false
   *
   * expected: <true> but was: <false>
   * Comparison Failure:
   * Expected :true
   * Actual   :false
   *
   * org.opentest4j.MultipleFailuresError: Multiple Failures (2 failures)
   * 	org.opentest4j.AssertionFailedError: expected: <true> but was: <false>
   * 	org.opentest4j.AssertionFailedError: expected: <true> but was: <false> */

  @Test
  public void assertj() {
    assertSoftly(softly -> {
      softly.assertThat(ACTUAL).isNotBlank();
      softly.assertThat(ACTUAL).startsWith("G");
      softly.assertThat(ACTUAL).endsWith("Power Ranger");
    });
  }
  /* java.lang.AssertionError:
   * Expecting actual:
   *   "White Ranger"
   * to start with:
   *   "G"
   *
   * java.lang.AssertionError:
   * Expecting actual:
   *   "White Ranger"
   * to end with:
   *   "Power Ranger"
   *
   * org.assertj.core.error.AssertJMultipleFailuresError:
   * Multiple Failures (2 failures)
   * -- failure 1 --
   * Expecting actual:
   *   "White Ranger"
   * to start with:
   *   "G"
   *
   * -- failure 2 --
   * Expecting actual:
   *   "White Ranger"
   * to end with:
   *   "Power Ranger" */
```

It can be argued that the library one uses for writing assertions is a matter
of personal taste, but which failure message would you like to see in the
reports or even the logs when's your job to find out what went wrong? Let me
guess: AssertJ's, right?

This is just the tip of the iceberg in regards to specialized assertions
methods and the failure messages they produce. You can go to the official
documentation and go head first into it. But before you do that, let's see how
each of these libraries make possible to enrich assertions' messages:

```java
  public static final String EXPECTED = "Green Ranger";

  public static final String ACTUAL = "White Ranger";

  @Test
  public void junit5() {
    assertAll("verify the Power Ranger's identity",
      () -> assertNotNull(ACTUAL, "it must not be null"),
      () -> assertFalse(ACTUAL.isBlank(), "it must not be blank"),
      () -> assertTrue(ACTUAL.startsWith("G"), "it should start with a 'G'"),
      () -> assertTrue(ACTUAL.endsWith("Power Ranger"), "it should be a Power Ranger"));
  }
  /* it should start with a 'G' ==> expected: <true> but was: <false>
   * Comparison Failure:
   * Expected :true
   * Actual   :false
   *
   * it should be a Power Ranger ==> expected: <true> but was: <false>
   * Comparison Failure:
   * Expected :true
   * Actual   :false
   *
   * org.opentest4j.MultipleFailuresError: verify the Power Ranger's identity (2 failures)
   * 	org.opentest4j.AssertionFailedError: it should start with a 'G' ==> expected: <true> but was: <false>
   * 	org.opentest4j.AssertionFailedError: it should be a Power Ranger ==> expected: <true> but was: <false> */

  @Test
  public void assertj() {
    assertSoftly(softly -> {
      softly.assertThat(ACTUAL)
            .describedAs("it must not be null nor blank")
            .withFailMessage("it instead is %s", ACTUAL)
            .isNotBlank();

      softly.assertThat(ACTUAL)
            .describedAs("it should start with a 'G'")
            .withFailMessage("it stars with '%s'", ACTUAL.substring(0, 1))
            .startsWith("G");

      softly.assertThat(ACTUAL)
            .describedAs("it should be a Power Ranger")
            .withFailMessage("it ends with '%s'", ACTUAL.substring(ACTUAL.length() / 2))
            .endsWith("Power Ranger");
    });
  }
  /* java.lang.AssertionError: [it should start with a 'G'] it stars with 'W'
   * at Cmp04Messages.lambda$assertj$4(Cmp04Messages.java:37)
   *
   * java.lang.AssertionError: [it should be a Power Ranger] it ends with 'Ranger'
   * at Cmp04Messages.lambda$assertj$4(Cmp04Messages.java:42)
   *
   * org.assertj.core.error.AssertJMultipleFailuresError:
   * Multiple Failures (2 failures)
   * -- failure 1 --
   * [it should start with a 'G'] it stars with 'W'
   * at Cmp04Messages.lambda$assertj$4(Cmp04Messages.java:37)
   * -- failure 2 --
   * [it should be a Power Ranger] it ends with 'Ranger'
   * at Cmp04Messages.lambda$assertj$4(Cmp04Messages.java:42) */
```

I ask you again, which failure message would be better to help you deeply
understand why that assertion is now unexpectedly failing? Which lines of code
would you rather spend your time reading and grasping? I think I know your
inclinations.

Hopefully, this article has made a strong case for you to consider putting a
little more of time, thought, and effort at creating the assertions for your
very important tests. As well as encouraging you to incorporate a library like
AssertJ into your projects.
