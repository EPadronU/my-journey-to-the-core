# To The Core - The YT Channel

## :::::::::: What is this about? ::::::::::

The idea with [this
channel](https://www.youtube.com/channel/UCkC_oadmIjO5bnJn-NQKl-A) is to
complement my [blog](https://gitlab.com/EPadronU/my-journey-to-the-core) and
[LinkedIn
articles](https://www.linkedin.com/in/edinson-e-padr%C3%B3n-urdaneta-10580487/detail/recent-activity/posts/)
for documenting my adventures along the path I walk every single day as a
computer scientist. Here I'll share my knowledge in a more practical and visual
way, giving back to a community that has helped me so much. Hopefully, you at
the other side of the screen, can join me in my adventures and become better
programmers together. I'm sure you will discover something you didn't know
minutes ago at the least.

## Series

### [My Java SE Tech Stack](https://www.youtube.com/playlist?list=PLt35-TchTYnGfa54kE5_KIenM8IoGXSKC)

Few days after the 2020 quarantine started, came to me a burning idea of
improving my knowledge on the core tools I've been using for so long, yet I
know very little in depth. So, in order to do that, I committed to myself to
read as much of the official documentation I reasonably should in order to have
a decent understanding of such fundamental technologies.

No too far into my journey, it occurred to me that it would be very exciting
and valuable to put together a development stack with the tools I was reading
about, plus learning those I've never work with but would be awesome to have in
my toolbox.

This
[playlist](https://www.youtube.com/playlist?list=PLt35-TchTYnGfa54kE5_KIenM8IoGXSKC)
contains the videos showcasing the knowledge I gained at the end of several
rewarding and exciting weeks of a lot of reading, toying, tweaking, and
(re)discovering of what I think is an amazingly orthogonal, yet cohesive set of
technologies that I want to use in my future endeavors.

- [![Setting up the environment](./assets/images/getting-the-tools.png)](https://youtu.be/CPMICWHI7B4)
- [![Maven's fundamentals](./assets/images/creating-the-project.png)](https://youtu.be/WQqe1fZKr18)
- [![Creating the project](./assets/images/speed-coding.png)](https://youtu.be/D5qEeDcP_dk)
- [![Log4j's fundamentals](./assets/images/logging-the-right-way.png)](https://youtu.be/9_4wxx0ipwg)
- [![JUnit5's fundamentals](./assets/images/testing-made-fun.png)](https://youtu.be/dzAeHMYo-wA)
- [![AssertJ's fundamentals](./assets/images/beautiful-assertions-with-assertj.png)](https://youtu.be/0AyUoiMw48o)
- [![PIT's fundamentals](./assets/images/mutation-testing.png)](https://youtu.be/uNcFH9ho5fU)
- [![Hibernate Validators's fundamentals](./assets/images/validations-the-right-way-with-hibernate-validator.png)](https://youtu.be/7mtNP5zPTVA)
- [![AspectJ: Pragmatic usages](./assets/images/flow-tracing-and-validations-revisited-with-aspectj.png)](https://youtu.be/C8aBDitgQTQ)
- [![Allure Framework's beautiful reports](./assets/images/next-level-test-reports-with-allure-framework.png)](https://youtu.be/piRkjoPib4Y)
