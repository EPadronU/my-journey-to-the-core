# My GNU/Linux adventures

Here, I'm going to put together some of the stuff I've learned about GNU/Linux
systems.

I'll start by going through the [_Basic System Software_][lfs-basic-sys-soft]
recommended by the [Linux From Scratch (LFS) book][lfs], but from the
perspective of the distribution I'm currently using, which is [KDE Neon][neon].

## Table of content

{{TOC}}

## Linux From Scratch

### Tools

In order to find out what packages provide the binaries as well as the files
suggested by the [LFS book][lfs], I opted to use the following commands:

* To know which package provides a particular file:
    ```bash
    apt-file search </path/to/file>
    ```
* To know what files are provided by a particular package:
    ```bash
    apt-file list <package-name>
    ```
* Just out of curiosity, what (installed) packages depend on another one
    ```bash
    apt-cache rdepends --installed <package>
    ```

### Software/packages

#### netbase (v6.1)

_«Basic TCP/IP networking system»_

##### /etc/ethertypes

Contains the most common protocols used as the **EtherType** of a **Ethernet
frame**.

_"For example, an EtherType value of `0x0800` signals that the frame contains
an IPv4 datagram. Likewise, an EtherType of `0x0806` indicates an ARP frame,
`0x86DD` indicates an IPv6 frame and `0x8100` indicates the presence of an IEEE
802.1Q tag."_\ [^ethernet_ii]

```conf
...
# <name>        <hexnumber> <alias1>...<alias35> # Comment
#
IPv4            0800    ip ip4  # IP (IPv4)
X25             0805
ARP             0806    ether-arp # Address Resolution Protocol
FR_ARP          0808            # Frame Relay ARP [RFC1701]
BPQ             08FF            # G8BPQ AX.25 over Ethernet
TRILL           22F3            # TRILL [RFC6325]
L2-IS-IS        22F4            # TRILL IS-IS [RFC6325]
TEB             6558            # Transparent Ethernet Bridging [RFC1701]
RAW_FR          6559            # Raw Frame Relay [RFC1701]
RARP            8035            # Reverse ARP [RFC903]
ATALK           809B            # Appletalk
AARP            80F3            # Appletalk Address Resolution Protocol
802_1Q          8100    8021q 1q 802.1q dot1q # VLAN tagged frame [802.1q]
IPX             8137            # Novell IPX
NetBEUI         8191            # NetBEUI
IPv6            86DD    ip6     # IP version 6
PPP             880B            # Point-to-Point Protocol
MPLS            8847            # MPLS [RFC5332]
...
```

Protocol[^protocol]
: A protocol is a standard set of rules that allow electronic devices to
communicate with each other. These rules include what type of data may be
transmitted, what commands are used to send and receive data, and how data
transfers are confirmed.

Octet (computing)[^octet]
: The octet is a unit of digital information in computing and
telecommunications that consists of eight bits. The term is often used when the
term byte might be ambiguous, as the byte has historically been used for
storage units of a variety of sizes.

Ethernet frame[^ethernet_frame]
: Is a data link layer protocol data unit and uses the underlying Ethernet
physical layer transport mechanisms.

EtherType[^ether_type]
: Is a two-octet field in an Ethernet frame. It is used to indicate which
protocol is encapsulated in the payload of the frame and is used at the
receiving end by the data link layer to determine how the payload is processed.

##### /etc/protocols §

Contains some [IP (Internet Protocol)][ip] protocols officially assigned by
[IANA][iana], and their corresponding protocol-number.

>In the Internet Protocol version 4 (IPv4) there is a field called "Protocol"
>to identify the next level protocol. This is an 8 bit field. In Internet
>Protocol version 6 (IPv6), this field is called the "Next Header"
>field.[^protocol_number]

```conf
...
ip      0       IP              # internet protocol, pseudo protocol number
hopopt  0       HOPOPT          # IPv6 Hop-by-Hop Option [RFC1883]
icmp    1       ICMP            # internet control message protocol
igmp    2       IGMP            # Internet Group Management
ggp     3       GGP             # gateway-gateway protocol
ipencap 4       IP-ENCAP        # IP encapsulated in IP (officially ``IP'')
st      5       ST              # ST datagram mode
tcp     6       TCP             # transmission control protocol
egp     8       EGP             # exterior gateway protocol
igp     9       IGP             # any private interior gateway (Cisco)
pup     12      PUP             # PARC universal packet protocol
udp     17      UDP             # user datagram protocol
hmp     20      HMP             # host monitoring protocol
xns-idp 22      XNS-IDP         # Xerox NS IDP
rdp     27      RDP             # "reliable datagram" protocol
iso-tp4 29      ISO-TP4         # ISO Transport Protocol class 4 [RFC905]
dccp    33      DCCP            # Datagram Congestion Control Prot. [RFC4340]
xtp     36      XTP             # Xpress Transfer Protocol
ddp     37      DDP             # Datagram Delivery Protocol
idpr-cmtp 38    IDPR-CMTP       # IDPR Control Message Transport
...
```

OSI (Open Systems Interconnection) model[^osi]
: The OSI model was created by the [ISO][iso] to help standardize communication
between computer systems. It divides communications into seven different
layers, which each include multiple hardware standards, protocols, or other
types of services.

##### /etc/rpc

_"This file contains user readable names that can be used in place of rpc
program numbers."_

RPC program numbers that must be unique across all networks are assigned by the
[Internet Assigned Number Authority (IANA)][iana].

```conf
...
portmapper      100000  portmap sunrpc
rstatd          100001  rstat rstat_svc rup perfmeter
rusersd         100002  rusers
nfs             100003  nfsprog
ypserv          100004  ypprog
mountd          100005  mount showmount
ypbind          100007
walld           100008  rwall shutdown
yppasswdd       100009  yppasswd
etherstatd      100010  etherstat
rquotad         100011  rquotaprog quota rquota
sprayd          100012  spray
3270_mapper     100013
rje_mapper      100014
selection_svc   100015  selnsvc
database_svc    100016
rexd            100017  rex
alis            100018
sched           100019
llockmgr        100020
...
```

RPC (Remote Procedure Call)[^rpc]
: The idea of remote procedure calls (hereinafter called RPC) is quite simple.
It is based on the observation that procedure calls are a well-known and well-
understood mechanism for transfer of control and data within a program running
on a single computer. Therefore, it is proposed that this same mechanism be
extended to provide for transfer of control and data across a communication
network. When a remote procedure is invoked, the calling environment is
suspended, the parameters are passed across the network to the environment
where the procedure is to execute (which we will refer to as the callee), and
the desired procedure is executed there. When the procedure finishes and
produces its results, the results are passed backed to the calling environment,
where execution resumes as if returning from a simple single-machine call.
While the calling environment is suspended, other processes on that machine may
(possibly) still execute (depending on the details of the parallelism of that
environment and the RPC implementation).

##### /etc/services §

_"Provides a mapping between friendly textual names for internet services_
\[(application layer)]_, and their underlying assigned port numbers and
protocol types."_\ [^services_i]

_"Service names and port numbers are used to distinguish between different
services that run over transport protocols such as TCP, UDP, DCCP, and SCTP."_
[^services_ii]

```conf
...
tcpmux          1/tcp                           # TCP port service multiplexer
echo            7/tcp
echo            7/udp
discard         9/tcp           sink null
discard         9/udp           sink null
systat          11/tcp          users
daytime         13/tcp
daytime         13/udp
netstat         15/tcp
qotd            17/tcp          quote
chargen         19/tcp          ttytst source
chargen         19/udp          ttytst source
ftp-data        20/tcp
ftp             21/tcp
fsp             21/udp          fspd
ssh             22/tcp                          # SSH Remote Login Protocol
telnet          23/tcp
smtp            25/tcp          mail
time            37/tcp          timserver
time            37/udp          timserver
...
```

Service[^service]
: The term service refers to a software functionality or a set of software
functionalities (such as the retrieval of specified information or the
execution of a set of operations) with a purpose that different clients can
reuse for different purposes, together with the policies that should control
its usage (based on the identity of the client requesting the service, for
example).

Process[^process]
: A process is a program that is running on your computer.

File descriptor[^fd]
: In Unix and Unix-like computer operating systems, a file descriptor (FD, less
frequently fildes) is a unique identifier for a file or other input/output
resource, such as a pipe or network socket.

Socket[^socket]
: When a computer program needs to connect to a local or wide area network such
as the Internet, it uses a software component called a socket. The socket opens
the network connection for the program, allowing data to be read and written
over the network.

Port number[^port]
: Is a 16-bit unsigned integer, thus ranging from 0 to 65535. A process
associates its input or output channels via an internet socket, which is a type
of file descriptor, associated with a transport protocol, an IP address, and a
port number. This is known as binding. A socket is used by a process to send
and receive data via the network. The operating system's networking software
has the task of transmitting outgoing data from all application ports onto the
network, and forwarding arriving network packets to processes by matching the
packet's IP address and port number to a socket. Common application failures,
sometimes called port conflicts, occur when multiple programs attempt to use
the same port number on the same IP address with the same protocol.

#### libc-bin (v2.31-0ubuntu9)

_«GNU C Library: Binaries»_

>The GNU C Library project provides the core libraries for the GNU system and
>GNU/Linux systems, as well as many other systems that use Linux as the kernel.
>These libraries provide critical APIs including ISO C11, POSIX.1-2008, BSD,
>OS-specific APIs and more. These APIs include such foundational facilities as
>`open`, `read`, `write`, `malloc`, `printf`, `getaddrinfo`, `dlopen`,
>`pthread_create`, `crypt`, `login`, `exit` and more.
>
>The GNU C Library is designed to be a backwards compatible, portable, and high
>performance ISO C library.[^libc]

##### catchsegv §

_"Can be used to create a stack trace when a program terminates with a
segmentation fault"_

_"The output is the content of registers, plus a backtrace. Basically you call
your program and its arguments as the arguments to catchsegv."_

```bash
$ catchsegv ./segfault | head -20
*** Segmentation fault
Register dump:

 RAX: 000056000197d005   RBX: 000056000197c150   RCX: 000056000197c150
 RDX: 00007ffd78509308   RSI: 00007ffd785092f8   RDI: 0000000000000001
 RBP: 00007ffd78509200   R8 : 0000000000000000   R9 : 00007fb117585d50
 R10: 00007fb11756b858   R11: 00007fb117547be0   R12: 000056000197c040
 R13: 00007ffd785092f0   R14: 0000000000000000   R15: 0000000000000000
 RSP: 00007ffd78509200

 RIP: 000056000197c144   EFLAGS: 00010206

 CS: 0033   FS: 0000   GS: 0000

 Trap: 0000000e   Error: 00000006   OldMask: 00000000   CR2: 0197d005

 FPUCW: 0000037f   FPUSW: 00000000   TAG: 00000000
 RIP: 00000000   RDP: 00000000

 ST(0) 0000 0000000000000000   ST(1) 0000 0000000000000000
```

Data structure[^data_structure]
: Is a collection of data values, the relationships among them, and the
functions or operations that can be applied to the data.

Subroutine[^subrutine]
: Is a sequence of program instructions that performs a specific task, packaged
as a unit. This unit can then be used in programs wherever that particular task
should be performed.

Call stack[^call_stack]
: Is a data structure that stores information about the active subroutines of a
computer program.

Stack trace (also called stack backtrace or stack traceback)[^stack_trace]
: Is a report of the active stack frames at a certain point in time during the
execution of a program.

Segmentation fault[^segfault]
: A segmentation fault (often shortened to segfault) or access violation is a
fault, or failure condition, raised by hardware with memory protection,
notifying an operating system (OS) the software has attempted to access a
restricted area of memory (a memory access violation).

##### getconf §

_"Displays [...] configuration variables for the current system and their
values."_

```bash
$ getconf -a | head -20
LINK_MAX                           65000
_POSIX_LINK_MAX                    65000
MAX_CANON                          255
_POSIX_MAX_CANON                   255
MAX_INPUT                          255
_POSIX_MAX_INPUT                   255
NAME_MAX                           255
_POSIX_NAME_MAX                    255
PATH_MAX                           4096
_POSIX_PATH_MAX                    4096
PIPE_BUF                           4096
_POSIX_PIPE_BUF                    4096
SOCK_MAXBUF
_POSIX_ASYNC_IO
_POSIX_CHOWN_RESTRICTED            1
_POSIX_NO_TRUNC                    1
_POSIX_PRIO_IO
_POSIX_SYNC_IO
_POSIX_VDISABLE                    0
ARG_MAX                            2097152
```

##### getent §

_"Gets entries from Name Service Switch libraries"_

```bash
$ getent services ssh
ssh                   22/tcp
$ getent protocols ipv6
ipv6                  41 IPv6
$ getent hosts localhost
::1             localhost ip6-localhost ip6-loopback
```

Name Service Switch (NSS)[^nss]
: [...] connects the computer with a variety of sources of common configuration
databases and name resolution mechanisms. These sources include local operating
system files (such as `/etc/passwd`, `/etc/group`, and `/etc/hosts`), the
Domain Name System (DNS), the Network Information Service (NIS, NIS+), and
LDAP.

##### iconv §

_"Convert text from one character encoding to another"_

```bash
$ echo abc ß α € àḃç | iconv -f UTF-8 -t ASCII//TRANSLIT
abc ss ? EUR abc
```

Character encoding[^char_enc]
: In computing, data storage, and data transmission, character encoding is used
to represent a repertoire of characters by some kind of encoding system that
assigns a number to each character for digital representation.

##### iconvconfig §

_"Creates fast-loading iconv module configuration files"_

>The iconv function internally uses gconv modules to convert to and from a
>character set. A configuration file is used to determine the needed modules
>for a conversion. Loading and parsing such a configuration file would slow
>down programs that use iconv, so a caching mechanism is employed.
>
>The iconvconfig program reads iconv module configuration files and writes a
>fast-loading gconv module configuration cache file.

---

>The gconv-modules files are line-oriented text files, where each of the lines
>has one of the following formats:
>
>* If the first non-whitespace character is a # the line contains only comments
>  and is ignored.
>* Lines starting with alias define an alias name for a character set. Two more
>  words are expected on the line. The first word defines the alias name, and
>  the second defines the original name of the character set. The effect is
>  that it is possible to use the alias name in the fromset or toset parameters
>  of iconv\_open and achieve the same result as when using the real character
>  set name.
>
>    This is quite important as a character set has often many different names.
>    There is normally an official name but this need not correspond to the
>    most popular name. Besides this many character sets have special names
>    that are somehow constructed. For example, all character sets specified by
>    the ISO have an alias of the form ISO-IR-nnn where nnn is the registration
>    number. This allows programs that know about the registration number to
>    construct character set names and use them in iconv\_open calls.
>
>* Lines starting with module introduce an available conversion module. These
>  lines must contain three or four more words.
>
>    The first word specifies the source character set, the second word the
>    destination character set of conversion implemented in this module, and
>    the third word is the name of the loadable module. The filename is
>    constructed by appending the usual shared object suffix (normally .so) and
>    this file is then supposed to be found in the same directory the
>    gconv-modules file is in. The last word on the line, which is optional, is
>    a numeric value representing the cost of the conversion. If this word is
>    missing, a cost of 1 is assumed. The numeric value itself does not matter
>    that much; what counts are the relative values of the sums of costs for
>    all possible conversion paths.[^gconv]

```bash
$ tail -n +34 /usr/lib/x86_64-linux-gnu/gconv/gconv-modules | head -20
alias   ISO-IR-4//              BS_4730//
alias   ISO646-GB//             BS_4730//
alias   GB//                    BS_4730//
alias   UK//                    BS_4730//
alias   CSISO4UNITEDKINGDOM//   BS_4730//
module  BS_4730//               INTERNAL                ISO646          2
module  INTERNAL                BS_4730//               ISO646          2

alias   ISO-IR-121//            CSA_Z243.4-1985-1//
alias   ISO646-CA//             CSA_Z243.4-1985-1//
alias   CSA7-1//                CSA_Z243.4-1985-1//
alias   CA//                    CSA_Z243.4-1985-1//
alias   CSISO121CANADIAN1//     CSA_Z243.4-1985-1//
alias   CSA_Z243.419851//       CSA_Z243.4-1985-1//
module  CSA_Z243.4-1985-1//     INTERNAL                ISO646          2
module  INTERNAL                CSA_Z243.4-1985-1//     ISO646          2

alias   ISO-IR-122//            CSA_Z243.4-1985-2//
alias   ISO646-CA2//            CSA_Z243.4-1985-2//
alias   CSA7-2//                CSA_Z243.4-1985-2//
```

##### ldd §

_"ldd prints the shared objects (shared libraries) required by each program or
shared object specified on the command line."_

```bash
$ ldd /usr/bin/ls
        linux-vdso.so.1 (0x00007ffdc8d5b000)
        libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f38ee9b8000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f38ee7c6000)
        libpcre2-8.so.0 => /lib/x86_64-linux-gnu/libpcre2-8.so.0 (0x00007f38ee736000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f38ee730000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f38eea27000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f38ee70d000)
```

Shared libraries[^shared_libs]
: Are libraries that are loaded by programs when they start. When a shared
library is installed properly, all programs that start afterwards automatically
use the new shared library.

See also [ld](#ld-.so§)

##### ldconfig §

_"Configures the dynamic linker runtime bindings"_

>ldconfig creates the necessary links and cache to the most recent shared
>libraries found in the directories specified on the command line, in the file
>`/etc/ld.so.conf`, and in the trusted directories, `/lib` and `/usr/lib` (on
>some 64-bit architectures such as x86-64, `/lib` and `/usr/lib` are the
>trusted direc‐ tories for 32-bit libraries, while `/lib64` and `/usr/lib64`
>are used for 64-bit libraries).
>
>The cache is used by the run-time linker, ld.so or ld-linux.so.

```bash
$ hd /etc/ld.so.cache | tail -20
0001bf40  2d 67 6e 75 2f 6c 69 62  42 72 6f 6b 65 6e 4c 6f  |-gnu/libBrokenLo|
0001bf50  63 61 6c 65 2e 73 6f 2e  31 00 6c 69 62 42 72 6f  |cale.so.1.libBro|
0001bf60  6b 65 6e 4c 6f 63 61 6c  65 2e 73 6f 00 2f 6c 69  |kenLocale.so./li|
0001bf70  62 2f 78 38 36 5f 36 34  2d 6c 69 6e 75 78 2d 67  |b/x86_64-linux-g|
0001bf80  6e 75 2f 6c 69 62 42 72  6f 6b 65 6e 4c 6f 63 61  |nu/libBrokenLoca|
0001bf90  6c 65 2e 73 6f 00 6c 69  62 42 61 73 69 63 55 73  |le.so.libBasicUs|
0001bfa0  61 67 65 45 6e 76 69 72  6f 6e 6d 65 6e 74 2e 73  |ageEnvironment.s|
0001bfb0  6f 2e 31 00 2f 6c 69 62  2f 78 38 36 5f 36 34 2d  |o.1./lib/x86_64-|
0001bfc0  6c 69 6e 75 78 2d 67 6e  75 2f 6c 69 62 42 61 73  |linux-gnu/libBas|
0001bfd0  69 63 55 73 61 67 65 45  6e 76 69 72 6f 6e 6d 65  |icUsageEnvironme|
0001bfe0  6e 74 2e 73 6f 2e 31 00  6c 69 62 41 70 70 53 74  |nt.so.1.libAppSt|
0001bff0  72 65 61 6d 51 74 2e 73  6f 2e 32 00 2f 6c 69 62  |reamQt.so.2./lib|
0001c000  2f 78 38 36 5f 36 34 2d  6c 69 6e 75 78 2d 67 6e  |/x86_64-linux-gn|
0001c010  75 2f 6c 69 62 41 70 70  53 74 72 65 61 6d 51 74  |u/libAppStreamQt|
0001c020  2e 73 6f 2e 32 00 6c 64  2d 6c 69 6e 75 78 2d 78  |.so.2.ld-linux-x|
0001c030  38 36 2d 36 34 2e 73 6f  2e 32 00 2f 6c 69 62 2f  |86-64.so.2./lib/|
0001c040  78 38 36 5f 36 34 2d 6c  69 6e 75 78 2d 67 6e 75  |x86_64-linux-gnu|
0001c050  2f 6c 64 2d 6c 69 6e 75  78 2d 78 38 36 2d 36 34  |/ld-linux-x86-64|
0001c060  2e 73 6f 2e 32 00                                 |.so.2.|
0001c066
```

##### locale §

_"Prints various information about the current locale"_

```bash
$ locale -k LC_TIME | head -20
abday="dom;lun;mar;mié;jue;vie;sáb"
day="domingo;lunes;martes;miércoles;jueves;viernes;sábado"
abmon="ene;feb;mar;abr;may;jun;jul;ago;sep;oct;nov;dic"
mon="enero;febrero;marzo;abril;mayo;junio;julio;agosto;septiembre;octubre;noviembre;diciembre"
am_pm="AM;PM"
d_t_fmt="%a %d %b %Y %T"
d_fmt="%d/%m/%y"
t_fmt="%T"
t_fmt_ampm="%I:%M:%S %p"
era=
era_year=""
era_d_fmt=""
alt_digits=
era_d_t_fmt=""
era_t_fmt=""
time-era-num-entries=0
time-era-entries="d"
week-ndays=7
week-1stday=19971130
week-1stweek=1
```

locale
: A locale is a set of language and cultural rules. These cover aspects such as
language for messages, different character sets, lexicographic conventions, and
so on. A program needs to be able to determine its locale and act accordingly
to be portable to different cultures.

##### localedef §

_"Compile locale definition files"_

>Individual locales can be installed using the `localedef` program. E.g., the
>`localedef` command below combines the `/usr/share/i18n/locales/es_CO`
>charset-independent locale definition with the
>`/usr/share/i18n/charmaps/UTF-8.gz` charmap definition and appends the result
>to the `/usr/lib/locale/locale-archive` file.

```bash
$ localedef -i es_CO -f UTF-8 es_CO.UTF-8
$ tail -n +61 /usr/share/i18n/locales/es_CO | head -10
LC_MESSAGES
copy "es_ES"
END LC_MESSAGES

LC_MONETARY
int_curr_symbol      "COP "
currency_symbol      "$"
mon_decimal_point    ","
mon_thousands_sep    "."
mon_grouping         3;3
$ zcat /usr/share/i18n/charmaps/UTF-8.gz | tail -n +7 | head -10
% CHARMAP generated using utf8_gen.py
% alias ISO-10646/UTF-8
CHARMAP
<U0000>     /x00         NULL
<U0001>     /x01         START OF HEADING
<U0002>     /x02         START OF TEXT
<U0003>     /x03         END OF TEXT
<U0004>     /x04         END OF TRANSMISSION
<U0005>     /x05         ENQUIRY
<U0006>     /x06         ACKNOWLEDGE
```

Locale definition file
: Contains all the information that the `localedef` command needs to convert it
into the binary locale database.

Charmap
: A character set description (charmap) defines all available characters and
their encodings in a character set.

##### pcprofiledump ø

_"Dump information generated by PC profiling"_

##### pldd §

_"Lists dynamic shared objects used by running processes"_

```bash
$ sudo pldd $fish_pid
13721:  /usr/bin/fish
linux-vdso.so.1
/lib/x86_64-linux-gnu/libtinfo.so.6
/lib/x86_64-linux-gnu/libdl.so.2
/lib/x86_64-linux-gnu/libpcre2-32.so.0
/lib/x86_64-linux-gnu/libstdc++.so.6
/lib/x86_64-linux-gnu/libm.so.6
/lib/x86_64-linux-gnu/libpthread.so.0
/lib/x86_64-linux-gnu/libc.so.6
/lib64/ld-linux-x86-64.so.2
/lib/x86_64-linux-gnu/libgcc_s.so.1
```

See also [ldd](#ldd§)

##### sln ø

_"The `sln` program creates symbolic links."_

>Unlike the `ln` program, it is statically linked. This means that if for some
>reason the dynamic linker is not working, `sln` can be used to make symbolic
>links to dynamic libraries.

Symbolic link[^sym_link]
: Is a term for any file that contains a reference to another file or directory
in the form of an absolute or relative path and that affects pathname
resolution.

##### tzselect §

_"Asks the user about the location of the system and reports the corresponding
time zone description"_

>Note that `tzselect` will not actually change the timezone for you. Use
>'`dpkg-reconfigure tzdata`' to achieve this.

```bash
$ tzselect
Please identify a location so that time zone rules can be set correctly.
Please select a continent, ocean, "coord", or "TZ".
 1) Africa
 2) Americas
 3) Antarctica
 4) Asia
 5) Atlantic Ocean
 6) Australia
 7) Europe
 8) Indian Ocean
 9) Pacific Ocean
10) coord - I want to use geographical coordinates.
11) TZ - I want to specify the timezone using the Posix TZ format.
#?
```

Timezone[^tz]
: A region throughout which the same standard time is used.

##### zdump §

_"The time zone dumper"_

>The zdump program prints the current time in each timezone named on the
>command line.

```bash
$ zdump America/Caracas America/Los_Angeles America/New_York
America/Caracas      Mon Dec  6 17:20:32 2021 -04
America/Los_Angeles  Mon Dec  6 13:20:32 2021 PST
America/New_York     Mon Dec  6 16:20:32 2021 EST
```

##### zic §

_"The time zone compiler"_

>The zic program reads text from the file(s) named on the command line and
>creates the time conversion information files specified in this input.

```conf
  EXTENDED EXAMPLE
         Here  is  an extended example of zic input, intended to illustrate many of its
         features.  In this example, the EU rules are for the European  Union  and  for
         its predecessor organization, the European Communities.

           # Rule  NAME  FROM  TO    TYPE  IN   ON       AT    SAVE  LETTER/S
           Rule    Swiss 1941  1942  -     May  Mon>=1   1:00  1:00  S
           Rule    Swiss 1941  1942  -     Oct  Mon>=1   2:00  0     -
           Rule    EU    1977  1980  -     Apr  Sun>=1   1:00u 1:00  S
           Rule    EU    1977  only  -     Sep  lastSun  1:00u 0     -
           Rule    EU    1978  only  -     Oct   1       1:00u 0     -
           Rule    EU    1979  1995  -     Sep  lastSun  1:00u 0     -
           Rule    EU    1981  max   -     Mar  lastSun  1:00u 1:00  S
           Rule    EU    1996  max   -     Oct  lastSun  1:00u 0     -

           # Zone  NAME           UTOFF    RULES  FORMAT  [UNTIL]
           Zone    Europe/Zurich  0:34:08  -      LMT     1853 Jul 16
                                  0:29:46  -      BMT     1894 Jun
                                  1:00     Swiss  CE%sT   1981
                                  1:00     EU     CE%sT

           Link    Europe/Zurich  Europe/Vaduz
```

##### /usr/share/libc-bin/nsswitch.conf

_"Somehow the NSS code must be told about the wishes of the user. For this
reason there is the file '/etc/nsswitch.conf'. For each database, this file
contains a specification of how the lookup process should work."_

```conf
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files
group:          files
shadow:         files
gshadow:        files

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
```

See also [getent](#getent§)

#### libc-dev-bin (v2.31-0ubuntu9)

_«GNU C Library: Development binaries»_

##### gencat §

_"Generates message catalogues"_

```
$ This is a leading comment.
$quote "

$set SetOne
1 Message with ID 1.
two "   Message with ID \"two\", which gets the value 2 assigned"

$set SetTwo
$ Since the last set got the number 1 assigned this set has number 2.
4000 "The numbers can be arbitrary, they need not start at one."
```

The source can be found [here][message_catalog_file]

Message catalogs[^message_catalog]
: Are specially formatted files that contain text messages that can be
displayed by a program. To facilitate translation of messages into various
languages and locales, all messages displayed by a program are separated from
the program's code and stored in message catalog files. When a program needs to
display a message, the message is retrieved from the appropriate message
catalog file and printed or displayed on the screen. At times it is useful to
view all of the messages that can be displayed by a particular program.

##### mtrace §

_"Reads and interprets a memory trace file and displays a summary in
human-readable format"_

```bash
$ env MALLOC_TRACE=mtrace.dat ./memory-leak
$ mtrace mtrace.dat

Memory not freed:
-----------------
           Address     Size     Caller
0x000056373ad04690      0x1  at 0x56373a1cf1a9
```

Memory leak[^mem_leak]
: A memory leak is like a virtual oil leak in your computer. It slowly drains
the available memory, reducing the amount of free memory the system can use.
Most memory leaks are caused by a program that unintentionally uses up
increasing amounts of memory while it is running. This is typically a gradual
process that gets worse as the program remains open. If the leak is bad enough,
it can cause the program to crash or even make the whole computer freeze.

##### rpcgen

_"rpcgen is a tool that generates C code to implement an RPC protocol. The
input to rpcgen is a language similar to C known as RPC Language (Remote
Procedure Call Language)."_

```bash
# generates the five files: prot.h, prot_clnt.c, prot_svc.c, prot_xdr.c and
# prot_tbl.i.
$ rpcgen -T prot.x
```

##### sotruss §

_"Traces shared library procedure calls of a specified command"_

```bash
$ sotruss echo hello
           echo -> libc.so.6      :*getenv(0x55d3ce01c24d, 0x7ffdeb42b178, 0x7ffdeb42b190)
           echo -> libc.so.6      :*strrchr(0x7ffdeb42bf85, 0x2f, 0x55d3ce01c24d)
           echo -> libc.so.6      :*setlocale(0x6, 0x55d3ce01cf61, 0x55d3ce01c24d)
           echo -> libc.so.6      :*bindtextdomain(0x55d3ce01c23f, 0x55d3ce01c260, 0x0)
           echo -> libc.so.6      :*textdomain(0x55d3ce01c23f, 0x7f5ae74fb010, 0x0)
           echo -> libc.so.6      :*__cxa_atexit(0x55d3ce019190, 0x0, 0x55d3ce020008)
           echo -> libc.so.6      :*strcmp(0x7ffdeb42bf8a, 0x55d3ce01c272, 0x20)
           echo -> libc.so.6      :*strcmp(0x7ffdeb42bf8a, 0x55d3ce01c279, 0x2d)
           echo -> libc.so.6      :*fputs_unlocked(0x7ffdeb42bf8a, 0x7f5ae75286a0, 0x0)
           echo -> libc.so.6      :*__overflow(0x7f5ae75286a0, 0xa, 0x55d3ce24b5f4)
hello
           echo -> libc.so.6      :*__fpending(0x7f5ae75286a0, 0x0, 0x55d3ce019190)
           echo -> libc.so.6      :*fileno(0x7f5ae75286a0, 0x0, 0x55d3ce019190)
           echo -> libc.so.6      :*__freading(0x7f5ae75286a0, 0x0, 0x55d3ce019190)
           echo -> libc.so.6      :*__freading(0x7f5ae75286a0, 0x0, 0x55d3ce019190)
           echo -> libc.so.6      :*fflush(0x7f5ae75286a0, 0x0, 0x55d3ce019190)
           echo -> libc.so.6      :*fclose(0x7f5ae75286a0, 0x0, 0x0)
           echo -> libc.so.6      :*__fpending(0x7f5ae75285c0, 0x0, 0x7f5ae7528801)
           echo -> libc.so.6      :*fileno(0x7f5ae75285c0, 0x0, 0x7f5ae7528801)
           echo -> libc.so.6      :*__freading(0x7f5ae75285c0, 0x0, 0x7f5ae7528801)
           echo -> libc.so.6      :*__freading(0x7f5ae75285c0, 0x0, 0x7f5ae7528801)
           echo -> libc.so.6      :*fflush(0x7f5ae75285c0, 0x0, 0x7f5ae7528801)
           echo -> libc.so.6      :*fclose(0x7f5ae75285c0, 0x0, 0x0)
```

See also [ldd](#ldd§)

##### sprof §

_"Reads and displays shared object profiling data"_

_**Note**: there's an amazing example in the man page for sprof that I won't
reproduce here._

#### libnss-db (v2.2.3pre1-6build6)

_«NSS module for using Berkeley Databases as a naming service»_

##### makedb §

_"Creates a simple database from textual input"_

>This program is mainly used to generate database files for the `libnss_db.so`
>module for the Name Service Switch.

```bash
$ cd /var/lib/misc && make && ls
Makefile  group.db  passwd.db  protocols.db  rpc.db  services.db  shadow.db...
```

See also [getent](#getent§)

##### /var/lib/misc/Makefile

_"Makefile to (re-)generate db versions of system database files."_

Makefile[^makefile]
: Is a file containing a set of directives used by a make build automation tool
to generate a target/goal.

#### nscd (2.31-0ubuntu9.2)

_«GNU C Library: Micro Name Service Caching Daemon»_

##### nscd §

_"A daemon that provides a cache for the most common name service requests"_

>Nscd caches libc-issued requests to the Name Service. If retrieving NSS data
>is fairly expensive, nscd is able to speed up consecutive access to the same
>data dramatically and increase overall system performance.

Daemon[^daemon]
: In multitasking computer operating systems, a daemon is a computer program
that runs as a background process, rather than being under the direct control
of an interactive user.

See also [getent](#getent§)

#### xtrace (v1.4.0-1)

_«Trace communication between X client and server»_

##### xtrace §

_"Xtrace fakes an X server and forwards all connections to a real X server,
displaying the communication between clients in (well, theoretically) human
readable form."_

```bash
$ xtrace
$ env DISPLAY=:9 xclock
# or
$ xtrace xclock
```

X Window System (X11, or simply X)[^x]
: Is a windowing system for bitmap displays, common on Unix-like operating
systems. X provides the basic framework for a GUI environment: drawing and
moving windows on the display device and interacting with a mouse and keyboard.
X does not mandate the user interface – this is handled by individual programs.
As such, the visual styling of X-based environments varies greatly; different
programs may present radically different interfaces.

X Client & X Server[^x]
: The X server is typically the provider of graphics resources and
keyboard/mouse events to X clients, meaning that the X server is usually
running on the computer in front of a human user, while the X client
applications run anywhere on the network and communicate with the user's
computer to request the rendering of graphics content and receive events from
input devices including keyboards and mice.

#### libc6 (v2.31-0ubuntu9)

_«GNU C Library: Shared libraries»_

##### ld-\*.so §

_"The programs `ld.so` and `ld-linux.so*` find and load the shared objects
(shared libraries) needed by a program, prepare the program to run, and then
run it."_

_"Linux binaries require dynamic linking (linking at run time) unless the
`-static` option was given to ld during compilation."_

_"The program `ld.so` handles `a.out` binaries, a binary format used long ago.
The program `ld-linux.so*` (`/lib/ld-linux.so.1` for libc5,
`/lib/ld-linux.so.2` for glibc2) handles binaries that are in the more modern
ELF format. Both programs have the same behavior, and use the same support
files and programs."_

_"The dynamic linker can be run either indirectly by running some dynamically
linked program or shared object (in which case no command-line options to the
dynamic linker can be passed and, in the ELF case, the dynamic linker which is
stored in the .interp section of the program is executed) or directly by
running:"_

```bash
/lib/ld-linux.so.*  [OPTIONS] [PROGRAM [ARGUMENTS]]
```

```bash
$ /usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2 --list /usr/bin/echo
        linux-vdso.so.1 (0x00007ffe91de5000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fc299288000)
        /lib64/ld-linux-x86-64.so.2 => /usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2 (0x00007fc299489000)
```

ELF[^elf]
: Executable and Linkable Format (ELF, formerly named Extensible Linking
Format), is a common standard file format for executable files, object code,
shared libraries, and core dumps.

    ![Structure of an ELF file with key entries highlighted][elf_img]

##### libBrokenLocale §

_"Used internally by Glibc as a gross hack to get broken programs (e.g., some
Motif applications) running."_

Motif[^motif]
: Motif refers to both a graphical user interface (GUI) specification and the
widget toolkit for building applications that follow that specification under
the X Window System on Unix and Unix-like operating systems.

##### libSegFault §

_"The segmentation fault signal handler, used by `catchsegv`"_

See also [catchsegv](#catchsegv§)

##### libanl §

_"An asynchronous name lookup library"_

##### libc §

_"The main C library"_

##### libdl §

_"Dummy library containing no functions. Previously was the dynamic linking
interface library, whose functions are now in libc"_

See also [ld](#ld-.so§)

##### libm §

_"The mathematical library"_

##### libmvec

_"The vector math library"_

##### libmemusage §

_"Used by `memusage` to help collect information about the memory usage of a
program"_

```bash
$ memusage --data=memusage.dat ./a.out
           ...
           Memory usage summary: heap total: 45200, heap peak: 6440, stack peak: 224
                   total calls  total memory  failed calls
            malloc|         1           400             0
           realloc|        40         44800             0  (nomove:40, dec:19, free:0)
            calloc|         0             0             0
              free|         1           440
           Histogram for block sizes:
             192-207             1   2% ================
           ...
            2192-2207            1   2% ================
            2240-2255            2   4% =================================
            2832-2847            2   4% =================================
            3440-3455            2   4% =================================
            4032-4047            2   4% =================================
            4640-4655            2   4% =================================
            5232-5247            2   4% =================================
            5840-5855            2   4% =================================
            6432-6447            1   2% ================
```

The source can be found [here][memusage_example]

##### libnsl §

_"The network services library, now deprecated"_

##### libnss\_\* §

_"The Name Service Switch libraries, containing functions for resolving host
names, user names, group names, aliases, services, protocols, etc."_

See also [getent](#getent§)

##### libpcprofile §

_"Can be preloaded to PC profile an executable"_

##### libpthread §

_"Dummy library containing no functions. Previously contained functions
providing most of the interfaces specified by the POSIX.1b Realtime Extension,
now the functions are in libc"_

POSIX.1b Realtime Extension[^realtime_extension]
: Comprehend features that allow X/Open realtime systems to reliably contain a
cohesive set of features for realtime applications.

##### libresolv §

_"Contains functions for creating, sending, and interpreting packets to the
Internet domain name servers"_

Domain name[^domain_name]
: A domain name is a unique name that identifies a website.

IP address[^ip_address]
: An IP address, or simply an "IP", is a unique address that identifies a
device on the Internet or a local network. It allows a system to be recognized
by other systems connected via the Internet protocol.

DNS (Domain Name System)[^dns]
: Serve as memorizable names for websites and other services on the Internet.
However, computers access Internet devices by their IP addresses. DNS
translates domain names into IP addresses, allowing you to access an Internet
location by its domain name.

##### librt §

_"Contains functions providing most of the interfaces specified by the POSIX.1b
Realtime Extension"_

See also [libpthread](#libpthread§)

##### libthread\_db §

_"Contains functions useful for building debuggers for multi-threaded
programs"_

Bug[^bug]
: Is an error in a software program. It may cause a program to unexpectedly
quit or behave in an unintended manner.

Debugger[^debugger]
: A debugger tells the programmer what types of errors it finds and often marks
the exact lines of code where the bugs are found. Debuggers also allow
programmers to run a program step by step so that they can determine exactly
when and why a program crashes.

Scheduler[^scheduler]
: Scheduling is the action of assigning resources to perform tasks. The
resources may be processors, network links or expansion cards. The tasks may be
threads, processes or data flows. The scheduling activity is carried out by a
process called scheduler.

Thread[^thread]
: A thread of execution is the smallest sequence of programmed instructions
that can be managed independently by a scheduler, which is typically a part of
the operating system.

##### libutil §

_"Dummy library containing no functions. Previously contained code for
“standard” functions used in many different Unix utilities. These functions are
now in libc"_

#### libc6-dev (v2.31-0ubuntu9)

_"«GNU C Library: Development Libraries and Header Files»"_

It provides a bunch of headers, static libraries, shared objects, and object
files from the GNU C library.

##### libg §

_"Dummy library containing no functions. Previously was a runtime library for
`g++`"_

G++
: GNU compiler for the C++ language.

##### libmcheck §

_"Turns on memory allocation checking when linked to"_

#### libcrypt-dev (v1:4.4.10-10ubuntu4)

_«libcrypt development files»_

##### libcrypt §

_"The cryptography library"_

Cryptography[^cryptography]
: Is the science of protecting information by transforming it into a secure
format. This process, called encryption, has been used for centuries to prevent
handwritten messages from being read by unintended recipients. Today,
cryptography is used to protect digital data. It is a division of computer
science that focuses on transforming data into formats that cannot be
recognized by unauthorized users.

#### zlib1g-dev (v1:1.2.11.dfsg-2ubuntu1)

_«compression library - development»_

##### libz §

_"Contains compression and decompression functions used by some programs"_

>zlib is designed to be a free, general-purpose, legally unencumbered -- that
>is, not covered by any patents -- lossless data-compression library for use on
>virtually any computer hardware and operating system. The zlib data format is
>itself portable across platforms. Unlike the LZW compression method used in
>Unix compress and in the GIF image format, the compression method currently
>used in zlib essentially never expands the data. (LZW can double or triple the
>file size in extreme cases.) zlib's memory footprint is also independent of
>the input data and can be reduced, if necessary, at some cost in compression.
>
>[...] the compression algorithm used in zlib is essentially the same as that
>in gzip and Zip [...].[^zlib]

Compression[^compression]
: Compression, or "data compression," is used to reduce the size of one or more
files. When a file is compressed, it takes up less disk space than an
uncompressed version and can be transferred to other systems more quickly.
Therefore, compression is often used to save disk space and reduce the time
needed to transfer files over the Internet.

#### bzip2 (v1.0.8-2)

_«high-quality block-sorting file compressor - utilities»_

>bzip2 is a freely available, patent free, high-quality data compressor. It
>typically compresses files to within 10% to 15% of the best available
>techniques (the PPM family of statistical compressors), whilst being around
>twice as fast at compression and six times faster at decompression.[^bzip2]

##### bunzip2 §

_"Decompresses bzipped files"_

```bash
$ bunzip2 -v textfile.bz2
  textfile.bz2: done
```

##### bzcat §

_"Decompresses files to stdout"_

```bash
$ bzcat textfile.bz2
file content
```

##### bzcmp §

_"Runs `cmp` on bzipped files "_

```bash
$ bzcat -v textfile.bz2 textfile2.bz2
  textfile.bz2:  file content
done
  textfile2.bz2: filx content
done
$ bzcmp -b textfile.bz2 textfile2.bz2
- /tmp/bzdiff.hw5EBHCdmP differ: byte 4, line 1 is 145 e 170 x
```

##### bzdiff §

_"Runs `diff` on bzipped files"_

```bash
$ bash bzip2/bzdiff -y -W20 textfile.bz2 textfile2.bz2
file  | filx
```

##### bzegrep §

_"Runs `egrep` on bzipped files"_

```bash
bzegrep '^file[\w ]+' textfile*.bz2
textfile.bz2:file content
```

##### bzexe

_"Compress executable files in place"_

```bash
$ bzexe ./bzdiff
  ./bzdiff:  2.115:1,  3.782 bits/byte, 52.73% saved, 2128 in, 1006 out.
$ file bzdiff
bzdiff: POSIX shell script executable (binary data)
$ file bzdiff~
bzdiff~: POSIX shell script, ASCII text executable
```

##### bzfgrep §

_"Runs `fgrep` on bzipped files"_

```bash
$ bzfgrep '^file' textfile*.bz2
# nothing, since `^` is interpreted literally
```

##### bzgrep §

_"Runs `grep` on bzipped files"_

```bash
$ bzgrep '^filx' textfile*.bz2
textfile2.bz2:filx content
```

##### bzip2 §

_"Compresses files using the Burrows-Wheeler[^burrows_wheeler] block sorting
text compression algorithm with Huffman coding; the compression rate is better
than that achieved by more conventional compressors using
Lempel-Ziv[^lempel_ziv] algorithms, like gzip."_

```bash
$ bzip2 -v textfile
  textfile:  0.250:1, 32.000 bits/byte, -300.00% saved, 13 in, 52 out.
```

##### bzip2recover §

_"Tries to recover data from damaged bzipped files"_

```bash
$ bzip2 -t 4k-damaged-image.jpg.bz2
  4k-damaged-image.jpg.bz2: data integrity (CRC) error in data
$ bzip2recover 4k-damaged-image.jpg.bz2
bzip2recover 1.0.8: extracts blocks from damaged .bz2 files.
bzip2recover: searching for block boundaries ...
   block 1 runs from 80 to 797641
   block 2 runs from 797690 to 1601491
   # ...
   block 51 runs from 40073248 to 40871480
   block 52 runs from 40871529 to 41566572
bzip2recover: splitting into blocks
   writing block 1 to `rec000014k-damaged-image.jpg.bz2' ...
   writing block 2 to `rec000024k-damaged-image.jpg.bz2' ...
   # ...
   writing block 51 to `rec000514k-damaged-image.jpg.bz2' ...
   writing block 52 to `rec000524k-damaged-image.jpg.bz2' ...
bzip2recover: finished
$ for x in $(ls rec*4k-damaged-image.jpg.bz2); do bzip2 -t $x; done
bzip2: rec000044k-damaged-image.jpg.bz2: data integrity (CRC) error in data
$ rm rec000044k-damaged-image.jpg.bz2
$ bzip2 -dc rec*4k-damaged-image.jpg.bz2 > 4k-damaged-image.jpg
$ file 4k-*image.jpg
4k-damaged-image.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI), density 72x72, segment length 16, progressive, precision 8, 6000x4000, components 3
4k-image.jpg:         JPEG image data, JFIF standard 1.01, resolution (DPI), density 72x72, segment length 16, progressive, precision 8, 6000x4000, components 3
```

##### bzless §

_"Runs `less` on bzipped files"_

```bash
bzless textfile.bz2
```

##### bzmore §

_"Runs `more` on bzipped files"_

```bash
bzmore textfile.bz2
```

#### libbz2-dev (v1.0.8-2)

_«high-quality block-sorting file compressor library - development»_

##### libbz2 §

_" The library implementing lossless, block-sorting data compression, using the
Burrows-Wheeler algorithm"_


<!-- Footnotes -->
[^bug]: <https://techterms.com/definition/bug>
[^burrows_wheeler]: <https://en.wikipedia.org/wiki/Burrows%E2%80%93Wheeler_transform>
[^bzip2]: <https://sourceware.org/bzip2>
[^call_stack]: <https://en.wikipedia.org/wiki/Call_stack>
[^char_enc]: <http://techterms.com/definition/characterencoding>
[^compression]: <https://techterms.com/definition/compression>
[^cryptography]: <https://techterms.com/definition/cryptography>
[^daemon]: <https://en.wikipedia.org/wiki/Daemon_(computing)>
[^data_structure]: <https://en.wikipedia.org/wiki/Data_structure>
[^debugger]: <https://techterms.com/definition/debugger>
[^dns]: <https://techterms.com/definition/dns>
[^domain_name]: <https://techterms.com/definition/domain_name>
[^elf]: <https://en.wikipedia.org/wiki/Executable_and_Linkable_Format>
[^ether_type]: <https://en.wikipedia.org/wiki/EtherType>
[^ethernet_frame]: <https://en.wikipedia.org/wiki/Ethernet_frame>
[^ethernet_ii]: <https://en.wikipedia.org/wiki/Ethernet_frame#Ethernet_II>
[^fd]: <https://en.wikipedia.org/wiki/File_descriptor>
[^gconv]: <https://www.gnu.org/software/libc/manual/html_node/glibc-iconv-Implementation.html>
[^ip_address]: <https://techterms.com/definition/ip_address>
[^lempel_ziv]: <https://en.wikipedia.org/wiki/LZ77_and_LZ78>
[^libc]: <https://www.gnu.org/software/libc>
[^makefile]: <https://en.wikipedia.org/wiki/Make_(software)#Makefile>
[^mem_leak]: <https://techterms.com/definition/memoryleak>
[^message_catalog]: <https://www.ibm.com/support/pages/message-catalogs>
[^motif]: <https://en.wikipedia.org/wiki/Motif_(software)>
[^nss]: <https://en.wikipedia.org/wiki/Name_Service_Switch>
[^octet]: <https://en.wikipedia.org/wiki/Octet_(computing)>
[^osi]: <https://techterms.com/definition/osi_model>
[^port]: <https://en.wikipedia.org/wiki/Port_(computer_networking)#Port_number>
[^process]: <https://techterms.com/definition/process>
[^protocol]: <https://techterms.com/definition/protocol>
[^protocol_number]: <https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml>
[^realtime_extension]: <https://unix.org/version2/whatsnew/realtime.html>
[^rpc]: <http://birrell.org/andrew/papers/ImplementingRPC.pdf>
[^scheduler]: <https://en.wikipedia.org/wiki/Scheduling_(computing)>
[^segfault]: <https://en.wikipedia.org/wiki/Segmentation_fault>
[^service]: <https://en.wikipedia.org/wiki/Service_(systems_architecture)>
[^services_i]: <https://linuxfromscratch.org/lfs/view/systemd/chapter08/iana-etc.html>
[^services_ii]: <https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml>
[^shared_libs]: <https://tldp.org/HOWTO/Program-Library-HOWTO/shared-libraries.html>
[^socket]: <https://techterms.com/definition/socket>
[^stack_Trace]: <https://en.wikipedia.org/wiki/Stack_trace>
[^subrutine]: <https://en.wikipedia.org/wiki/Subroutine>
[^sym_link]: <https://en.wikipedia.org/wiki/Symbolic_link>
[^thread]: <https://en.wikipedia.org/wiki/Thread_(computing)>
[^tz]: <https://www.thefreedictionary.com/time+zone>
[^x]: <https://en.wikipedia.org/wiki/X_Window_System>
[^zlib]: <https://zlib.net>

<!-- Links -->
[iana]: https://www.iana.org
[ip]: https://techterms.com/definition/ip
[iso]: https://techterms.com/definition/iso
[lfs-basic-sys-soft]: https://linuxfromscratch.org/lfs/view/systemd/chapter08/introduction.html
[lfs]: https://linuxfromscratch.org/lfs
[memusage_example]: https://man7.org/linux/man-pages/man1/memusage.1.html#EXAMPLES
[message_catalog_file]: https://www.gnu.org/software/libc/manual/html_node/The-message-catalog-files.html
[neon]: https://neon.kde.org

<!-- Images -->
[!elf_img]: https://upload.wikimedia.org/wikipedia/commons/e/e4/ELF_Executable_and_Linkable_Format_diagram_by_Ange_Albertini.png
