# Java SE Technology Stack

## Introduction

Few days after the 2020 quarantine started, came to me a burning idea of
improving my knowledge on the core tools I've been using for so long, yet I
know very little in depth. So, in order to do that, I committed to myself to
read as much of the official documentation I reasonably should in order to have
a decent understanding of such fundamental technologies.

No too far into my journey, it occurred to me that it would be very exciting and
valuable to put together a development stack with the tools I was reading
about, plus learning those I've never work with but would be awesome to have in
my toolbox.

The following list is the result of amazing, rewarding and exciting weeks of a
lot of reading, toying, tweaking, and (re)discovering of what I think is an
amazingly orthogonal, yet cohesive set of technologies that I want to use in my
future endeavors.

## The Stack

- [Maven](#maven)
- [Log4j](#log4j)
- [JUnit5](#junit5)
- [AssertJ](#assertj)
- [PIT](#pit)
- [Hibernate Validator](#hibernate-validator)
- [AspectJ](#aspectj)
- [Eclipse Collections](#eclipse-collections)
- [Allure Framework](#allure-framework)

### [Maven](https://maven.apache.org/)

#### My background story

As it happened with a lot of us, I was introduced to Java at college. At that
point of my life I used [Eclipse IDE](https://www.eclipse.org/ide/) and the
complexity of the software I developed back then was nothing compared to what I
do now, so just downloading a couple of JAR files and telling Eclipse where to
find them was enough.

---

I was so naive but life was so simple those days.

---

Few months after my first taste of Java, I was introduced to
[Python](https://www.python.org/) in an event about Open Source, I resisted a
little bit at the beginning since all I knew at the time was
[C](https://en.wikipedia.org/wiki/C_%28programming_language%29) and Java.
After giving the language a chance I learned to like it and two years passed
until I went back to the JVM world.

I grew tired of the downsides the dynamism that comes with languages like
Python, Ruby or JavaScript inevitably brings to the table, the lack of
type-safety and the _magic_, just to mention a couple of them. Nonetheless, at
my return I didn't pick Java right away. Since I was very used to Python's
brevity, expressiveness, flexibility, and ease to write, read and maintain I
picked [Apache Groovy](http://www.groovy-lang.org/) (it was part of
[Codehaus](https://www.javaworld.com/article/2892227/codehaus-the-once-great-house-of-code-has-fallen.html)
at the time) as my daily driver for the next year. Naturally, I choose
[Gradle](https://www.gradle.org/) as my build tool and everything was great...
Well, sort of.

Even though I liked the more modern look of Gradle (Groovy instead of XML - _I
know, I was still young and naive_), I must confess (_and this is a personal
opinion_) everything was a nightmare. The support in [Intellij
IDEA](https://www.jetbrains.com/idea/) was very lacking and glitchy, and in top
of that the IDE slowed down so much when working with Gradle that frustration
built up quite quickly. But the worst part of it was the lack of documentation
for even the simplest task, that much of the time required a hacky piece of
absurd code someone came with in [StackOverflow](https://stackoverflow.com/).

Those days were rough and they got worse when I jumped to the
[Kotlin](https://kotlinlang.org/) bandwagon at the beginning of 2016, since
Gradle's Kotlin support was not very good and less documentation on such a
support existed back then.

Despite Groovy and Kotlin being my source of joy and income (I was
working as a freelancer), the corporate world (no more freelancing until this
very day) made me use Java again. Fortunately, it was a Java with lambdas,
streams, and optionals, things that allowed me not to quit to so much of the
features I really loved in Groovy and still make me adore Kotlin. But even
though a lot of corporations use Gradle, the ones I've worked for the last
three years use Maven and Maven only.

At first, I thought it will be harsh to get used to it, that I couldn't deal
with the XML, the lack of power and flexibility a programming language like
Groovy or Kotlin gives us. Luckily, most of the time the
[POM](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html)
was already setup and the few times I needed to change or to introduce
something new I just had to google for a few minutes, copy-and-paste the
fragment of XML, tweak a little bit some parameters and that was it. The damn
thing worked perfectly! **The amount of documentation, and how smoothly Maven
works in IDEA was shockingly refreshing.**

#### The tool

Let's fast-forward 3.5 years, a couple of months after a world-wide event that
will change life as we knew it started. As I read Maven's documentation I get
more and more excited with how powerful, flexible, and easy is to deal with the
tasks that are related to the build of a project: dependency management,
running tests, report generation, code coverage, and so on.

Once you overcome the use of XML, which even if it sounds silly I have to admit
it is quite frightening at first but surprisingly easy to grasp in no time, you
have an incredible and powerful build tool that offers a lot out of the box,
doesn't get in the way, is fast (even when used in an IDE), has an amazing
documentation, is flexible, and can be easily extended with a plethora of
amazing plugins (and it's quite easy to make our owns).

---

Maven is a fantastic, versatile, extensible, fast, and easy-to-use piece of
software. It will be core to my toolbox from now on.

---


### [Log4j](https://logging.apache.org/log4j/2.x/)

#### My background story

Every single project I've been involved in has used
[SLF4J](http://www.slf4j.org/) with [Logback](http://logback.qos.ch/) (its
default implementation) for logging purposes. The historical reasons for this
are simple: Java lacked its own built-in logging capabilities until they were
introduced in version 1.4 in the form of
[`java.util.logging`](https://docs.oracle.com/en/java/javase/14/core/java-logging-overview.htm)
(JUL), but by the time this happened it was _too little too late_. _Too late_
due to the existence and use of [Log4j](https://logging.apache.org/log4j/1.2/)
for quite sometime and _too litle_ since JUL lacked a lot of the power Log4j
offered by then.

---

It is worth mentioning that I'm too young of a software developer for the days
when Log4j was the de-facto logging library for Java, not to mention when Java
1.4 was the current amazing iteration of a beloved programming language rather
than a headache for those who are yet maintaining pretty old legacy systems.

---

At some point down the road Ceki Gülcü, the creator of Log4j, driven by the
desire of improving upon its previous creation as well as the [Apache Commons
Logging](https://commons.apache.org/proper/commons-logging/) (previously known
as Jakarta Commons Logging or JCL), decided to start fresh splitting the API
from its implementation. [SLF4J](http://www.slf4j.org/) and
[Logback](http://logback.qos.ch/) were the result of such an endeavour.

We had a new de-facto logging library and from the same craft-master that gave
us the previous one. There wasn't a lot to think about when choosing how to do
logging in our projects, right? We just have to use SLF4J for the API, and for
the implementation (Logback most of the time, naturally) we can use whichever
fit our needs best and it can even be replaced in the future is necessary with
very little cost.

But like life, technology evolves but at a faster pace and [Log4j 2 was
born](https://www.ralphgoers.com/post/why-was-log4j-2-created) (Log4j for sake
of simplicity and since the 1.x branch reached end-of-life) out of needs SLF4J
couldn't satisfy at the time. And, oh boy! I couldn't be more grateful to
Ralph Goers for starting this project.

Even though SLF4J is catching up by means of the development being done in its
2.x branch since mid 2019, it is still in alpha at the moment I'm writing this.
So for now, I'll more than pleased to use Log4j and to push for its adoption in
all my future projects.

#### The tool

Log4j can be configured not only by XML and property files but also JSON and
even YAML can be used, it's [_garbage-free_ or _low
garbage_](https://logging.apache.org/log4j/2.x/manual/garbagefree.html) out of
the box in common configurations, allows us to create [custom log
levels](https://logging.apache.org/log4j/2.x/manual/customloglevels.html) that
are closer to the application's domain, make use of Java 8 lambdas for [_lazy
logging_](https://logging.apache.org/log4j/2.x/manual/api.html#Java_8_lambda_support_for_lazy_logging)
(which is the killer feature to me), supports [`Message`
objects](https://logging.apache.org/log4j/2.x/manual/messages.html) (rather
than just `String`'s) and lets us define our own.

---

Log4j is [impressively
customizable](https://logging.apache.org/log4j/2.x/manual/configuration.html),
[blazing fast](https://logging.apache.org/log4j/2.x/performance.html), and
integrates with a wide range of components like databases, containers, and
popular logging implementations (including those developed for SLF4J). It also
offers its own implementation, which I recommend in order to leverage all the
power Log4j has to offer.

---


### [JUnit5](https://junit.org/junit5/)

#### My background story

As I mentioned earlier in this document, my first taste of Java was at college
and testing was not a priority for me back then. After my adventures with
Python, I returned to the JVM through Groovy and made extensive use of
[Spock](http://spockframework.org/) which is a testing framework that leverage
Groovy's powers in order to offer us _"[a] beautiful and highly expressive
specification language"_. And I loved it!

When I transitioned to Kotlin, I looked for something that could fill Spock's
shoes and I ran into [Spek](https://www.spekframework.org/). At the beginning
of 2016, Spek was in its infancy (its 1.0 release was on September 1st of that
same year) and not only lacked a lot of the power that Spock got me used to but
it just wasn't ready for my testings needs. That's why I ended up choosing
[TestNG](https://testng.org/doc/index.html) as my testing engine.

---

It is worth noting that both, Spock and Spek are
[**specification**](http://www.betterspecs.org/) (a concept related to
[BDD](https://en.wikipedia.org/wiki/Behavior-driven_development) and
popularized by [RSpec](https://relishapp.com/rspec/), among others) frameworks.

---

TestNG was and obvious choice for me as I was very familiarized with it since
all the projects I'd worked with up until then (and even today) used it. When
I asked for the reasons behind the selection of TestNG (over JUnit, for
example), I found myself disappointed with _"it is what we know and use"_ as
the answer. Therefore, it's not a surprise that I want to believe the real
reasons were that with TestNG [Data-driven
testing](https://en.wikipedia.org/wiki/Data-driven_testing) was [easier to
achieve](https://automationrhapsody.com/data-driven-testing-junit-parameterized-tests/)
as well as parallel-test execution, plus the use of XML configuration files was
a flexible and simple way to create test suites.

But, yet again, we have to fast-forward to the early 2020s while I'm head-deep
in the JUnit5 documentation and I'm loving it.

#### The tool

JUnit5 is the next generation of the beloved unit testing framework, and has
been rewritten from the ground up. Similarly to SLF4J and Log4j,
[JUnit5](https://junit.org/junit5/docs/current/user-guide/#overview-what-is-junit-5)
separates the API from the implementation and even offers and engine for
backwards compatibility with tests based on JUnit 3.x and 4.x.

[Parallel-test
execution](https://junit.org/junit5/docs/current/user-guide/#writing-tests-parallel-execution)
is impressively simple to set up and choosing which test methods (or even
entire test classes) should or should not be run in parallel is as simple as it
can be. [Tagging
tests](https://junit.org/junit5/docs/current/user-guide/#writing-tests-tagging-and-filtering)
is straightforward and [_tag
expressions_](https://junit.org/junit5/docs/current/user-guide/#running-tests-tag-expressions)
give us a powerful way for filtering tests when we need to run a particular
subset of them. [Conditional test
execution](https://junit.org/junit5/docs/current/user-guide/#extensions-conditions),
granular [test execution
order](https://junit.org/junit5/docs/current/user-guide/#writing-tests-test-execution-order)
control, and [dependency injection of
metadata](https://junit.org/junit5/docs/current/user-guide/#writing-tests-dependency-injection)
on the tests themselves are awesome features that couldn't be easier to use.
But that's not all, JUnit5 also supports
[nested](https://junit.org/junit5/docs/current/user-guide/#writing-tests-nested),
[repeated](https://junit.org/junit5/docs/current/user-guide/#writing-tests-repeated-tests),
[parameterized (which enable **data-driven
testing**)](https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests),
and even
[**dynamic**](https://junit.org/junit5/docs/current/user-guide/#writing-tests-dynamic-tests)
tests, and with all of them we can declare a
[**timeout**](https://junit.org/junit5/docs/current/user-guide/#extensions-conditions)
that, if reached, will fail the test (for when we need to test compliance to a
[SLA](https://en.wikipedia.org/wiki/Service-level_agreement) for example).

---

JUnit5 is the most complete and powerful testing framework there is for Java.
It's **essential** for projects of any size that want to ensure that the
quality of their development meets their standards.

---


### [AssertJ](https://assertj.github.io/doc/)

#### The story

Although JUnit5 is an excellent testing framework, its assertion capabilities
lack expressiveness and fluency, and error messages when assertions fail are
not very informative but for the simplest of the cases. For these reasons, I
started looking at the top three assertion frameworks for Java:
[Hamcrest](http://hamcrest.org/JavaHamcrest/), [Truth](https://truth.dev/), and
[AssertJ](https://assertj.github.io/doc/). The following lines describe my
personal opinion on each of them and why I chose AssertJ as my daily driver
for assertions.

Hamcrest is the simplest of the pack, it is available in multiple programming
languages (like Python, Ruby, and even Swift), it offers an easy way for us to
create custom matchers for our system's domain objects, and it is used not only
for making assertions but also for setting expectations on mocking frameworks.
What's not to like? I'll tell you:

* There are very few matchers for even the most common Java classes, meaning
  that even at a very early stage in our adoption of Hamcrest, we'll have to
  create a bunch of custom matchers for our day-to-day needs, slowing us down
  significantly and introducing fragmentation and inconsistency, since every
  team in the whole world has to do this, creating their own matchers for the
  same classes that are common to all of us who develop software in Java. These
  are not good things, of course.

* Hamcrest's approach for assertions is not very fluent, so IDEs can't suggest
  the appropriate matchers for a given object.

 Hamcrest was a **big no** for me, what about the other two? Truth and AssertJ
 are [very similar](https://truth.dev/comparison#vs-assertj) and I think it
 would be a matter of personal taste to adopt one over the other. So, why did I
 pick AssertJ? There are three big reasons: the first is the fact that AssertJ
 does not bring transitive dependencies to our projects, unlike Truth, which
 introduces quite a few; the second one is the [assertions
 generator](https://joel-costigliola.github.io/assertj/assertj-assertions-generator.html)
 that provides us with a quick way to generate custom assertions for our domain
 objects; and last but not least, the power of AssertJ's API that even allows
 us to [extract bean's
 properties](https://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html#extracted-properties-assertion)
 and [filter elements in a
 collection](https://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html#filters)
 in order to have more grained control over what we are going to assert.

#### The tool

AssertJ brings to the table a sweet API that's not only easy to use but also
very convenient, since IDEs can help us by suggesting relevant assertions for a
given object. We can enrich assertions' error messages by [describing
them](https://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html#describe-assertion).
[_Late-failure_](https://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html#soft-assertions)
for gathering all error messages (rather than the first one) is fairly easy to
accomplish. [Using custom representations of
assertions](https://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html#custom-representation)
fine-tune how error messages are displayed, creating [custom
conditions](https://joel-costigliola.github.io/assertj/assertj-core-conditions.html)
to be asserted and [custom comparison
strategies](https://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html#custom-comparison-strategy)
for flexibility in comparing objects are just a few of the many wonderful
features AssertJ has to offer.

---

AssertJ is a very needed complement to JUnit5. It offers an incredibly
powerful, comprehensive, and fluent assertion API with very informative and
beautiful error messages that will greatly enhance and ease our testing and
debugging efforts.

---


### [PIT](https://pitest.org/)

#### The story

In 2016, shortly after I started learning Kotlin, I came across an [article on
_mutation
testing_](https://medium.com/@s4n1ty/experimenting-with-mutation-testing-and-kotlin-b515d77e85b5)
that caught my attention. The idea was simple but very powerful. How good are
our tests? Can they really detect flaws in the code or just tell us they
haven't detected one yet? Similar to the thought process triggered by the
question of _who police the police_, we should ponder **how can we test the
tests**?

Testing is as complex as development itself, but with a quite different
mindset. Rather than translating business logic into instructions a computer
can understand and follow as closely as the users expect the system to behave,
testing requires thinking of as many scenarios as they can be when someone or
something (another program, for example) interacts with our software. Even the
scenarios bordering on the absurd must be contemplated, because if they can
happen, we should be prepared since, on the contrary, the consequences could be
disastrous (personal information leaks, stolen money, lives lost).

Unfortunately, metrics like code coverage, while important, don't help with the
above situation. The amount of code exercised by the tests doesn't reveal a lot
about whether they are actually relevant for uncovering defects that are
dormant, waiting for an unlikely but still possible input combination to happen
and which will bring the entire system to its knees, or even worse, [expose
innocent people to lethal doses of
radiation](https://www.wired.com/2005/11/historys-worst-software-bugs/).

Even if there's no silver bullet, our testing efforts can be improved by making
use of mutation testing to introduce _mutations_ (changes in the code) and
verify whether the tests exercising those changes pass (the mutation
_survived_) or fail (the mutation was _killed_) to determine if such tests can
detect dormant defects in the software. And there's a tool for the JVM
multiverse called PIT that does the job incredibly well.

#### The tool

Setting up PIT in a project is a breeze. We just have to include it into our
[POM (Maven)](https://pitest.org/quickstart/maven/) or [build file
(Gradle)](https://gradle-pitest-plugin.solidsoft.info/) and that's it. Out of
the box, it brings sane defaults that will introduce a wide range of
[mutations](https://pitest.org/quickstart/mutators/) into our source code and
will execute only those tests that cause the generated mutation to run. Of
course, we can customize the mutations (or groups of them) to be generated, the
subset of the code to which the mutations should be applied, and even the
subset of the tests to be run. At the end of this
[process](https://pitest.org/faq/#how-does-pit-choose-which-tests-to-run), PIT
will give us an incredibly informative and beautiful
[report](https://youtu.be/9yG1c9Crnbk?t=1829) showing us the actual lines of
code that count with both, code and mutation coverage, describing which
mutations were killed and which ones survived (if any).

---

PIT provides us with valuable information on not only the code that our tests
exercise, but also on how relevant these tests are and how good they are for
catching flaws in the code. All of this done intelligently, systematically, and
automatically.

---


### [Hibernate Validator](https://hibernate.org/validator/)

#### The story

In 2014, I took Udacity's Software Debugging course
[(CS259)](https://www.udacity.com/course/software-debugging--cs259). The
lessons are so packed with information that over the years I have revisited my
notes over and over again just to make sure I'm not missing something on such a
fundamental subject. Among all the topics covered by Mr. Andreas Zeller,
**preconditions**, **postconditions**, and **invariants** were the ones that
continue to amaze me even today. These concepts are an essential part of the
[design by contract](https://en.wikipedia.org/wiki/Design_by_contract) approach
and this is how Zeller describes them:

- A _precondition_ is a group of assertions being called at the beginning of a
  function and which checks the properties of the arguments.
- A _postcondition_ is a group of assertions being called at the end of a
  function and which checks the result of it.
- An _invariant_ is a condition that always (at the beginning and at the end of
  each public method) hold for a data object.

---

While I agree they can behave as assertions, I prefer the term _constraints_,
since we can decide not to throw an exception when one of them is not fulfilled
(as assertions usually tend to do), and rather log the **violation**, for
example.

---

Most of us may be familiarized with the `@NotNull` annotation, the simplest
specimen we can find in the wild, which at least **documents** that a method's
argument shouldn't be `null`, or that the caller of a method shouldn't get
`null` as a value returned by it, depending on what is annotated: a method's
parameter or the method itself. `@NotNull` is just a tiny piece of the [Jakarta
Bean Validation (JBV) specification](https://beanvalidation.org/), with which
we can **express** preconditions, postconditions, and invariants.

But an specification is not enough, it just an API, a skeleton. We need the
muscles for actually performing the validations and find the violations (if
any) present in the system for the constraints we have defined on its domain
objects.

#### The tool

[Hibernate Validator](https://hibernate.org/validator/) is the **reference
implementation** of the Bean Validation specification and it's part of the
Hibernate umbrella (the [ORM](https://hibernate.org/orm/) being the most known
member). If you have used Spring + Hibernate, there's a good chance you have
used Hibernate Validator without knowing it. But don't worry, we can make use
of it outside JEE.

We are able to declare and validate constraints on
[fields](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#_field_level_constraints),
[properties](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#_property_level_constraints),
[container
elements](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#container-element-constraints),
[method's
parameters](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#_parameter_constraints),
[return
values](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#_parameter_constraints),
among others. And as a plus, all of these component's documentation gets
enriched with the annotations used for describing their respective constraints,
avoiding potential inconsistency that can be introduced by manually including
such information. Additionally, Hibernate Validator extends the [set of
constraints defined by
JBV](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#validator-defineconstraints-spec)
with a fan of [very convenient and useful
ones](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#validator-defineconstraints-hv-constraints),
supports [class-level
constraints](ihttps://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#section-class-level-constraints)
(which JBV lacks), and even provides a way to declare our own custom
constraints.

---

If following the **design by contract** approach, while having a orthogonal,
descriptive, simple, and powerful validation harness that not only ensures our
APIs receive their input as expected, but also verifies we are delivering on
our promise to our clients without doing either by ourselves is what we want,
then Hibernate Validator is definitely what we need.

---


### [AspectJ](https://www.eclipse.org/aspectj/)

#### The story

Object-oriented programming (OOP) languages like Java provide a set of features
(such as classes and packages) that allow for independence and modularization
of **concerns** (data persistence, graphical representation of information,
data serialization, business logic, among others) in our programs. But there
are [**cross-cutting**
concerns](https://en.wikipedia.org/wiki/Cross-cutting_concern) that, by their
very nature and due to language limitations, are scattered throughout the
system, impossible to encapsulate in a single modularization unit. We all are
familiarized with the simplest forms of these cross-cutting concerns: logging,
authentication, access-control and constraints validation. Fortunately, the
[Aspect Oriented Programming (AOP)
paradigm](https://en.wikipedia.org/wiki/Aspect-oriented_programming) was
created to make cross-cutting concerns easy to modularize, and
[AspectJ](https://www.eclipse.org/aspectj/) is just the tool for that.

#### The tool

AOP and, by extension AspectJ, introduce a couple of new concepts that we must
understand to take advantage of their full potential:

* [Aspects](https://www.eclipse.org/aspectj/doc/released/progguide/starting-aspectj.html#aspects)
  are the main _modularization units of crosscutting implementation_, just like
  classes to Java. And just like classes, they may contain methods and fields,
  but more importantly, aspects encapsulates pointcuts, advices, and inter-types
  declarations.
* [Join
  points](https://www.eclipse.org/aspectj/doc/released/progguide/language-joinPoints.html)
  are well-defined points in the program's execution such as method calls,
  method executions, object instantiations, constructor executions, field
  references, and handler executions.
* Similar to how a regular expression describes a set of strings, a
  [pointcut](https://www.eclipse.org/aspectj/doc/released/progguide/language-anatomy.html#pointcuts)
  describes or _picks out_ join points.
* [Advices](https://www.eclipse.org/aspectj/doc/released/progguide/starting-aspectj.html#advice)
  are blocks of code that are run at a certain point of the execution of a join
  point (before a constructor is called or after an exception is thrown in a
  method, for example).
* [Inter-type
  declarations](https://www.eclipse.org/aspectj/doc/released/progguide/starting-aspectj.html#inter-type-declarations)
  enable the alteration of the static aspects of the program, such as interface
  implementation, inheritance hierarchies and members of classes.

In short, AspectJ allows us to create _aspects_ to enrich the system's dynamic
structure by defining additional behavior (_advices_) at well-defined points
in the program's execution (_joint points_ pick up by _pointcuts_), as well as
altering its static structure through the implementation of an interface, the
extension of a class or the introduction of new members (_inter-type
declarations_). All Of this without touching the original code and whit an
opt-in mechanism that makes it easy to _plug-and-play_ aspects only when we
need them (just in development but not in production, for example).

Using AspectJ in our existing projects is quite simple to do. We just need to
include a [plugin](https://github.com/nickwongdev/aspectj-maven-plugin) into
our POM file and to create our _aspects_ in either AspectJ's language inside an
`.aj` file or Java with an [annotation-based development
style](https://www.eclipse.org/aspectj/doc/released/adk15notebook/ataspectj.html)
in a `.java` file.

---

Adopting AOP with AspectJ is the smartest thing we can do to modularize
cross-cutting concerns like **logging**, **authentication**,
**access-control**, and **constraints validation**. The power this paradigm
offers and the headaches in the form of error-prone, repetitive and manual work
that helps us avoid make AspectJ a valuable member of our toolbox.

---


### [Eclipse Collections](https://www.eclipse.org/collections/)

#### My background story

My days with Python, Groovy, and even Kotlin got me used to wonderful,
painless, easy-to-read, and fluent APIs for collections. And I have to admit
that even if things in the Java world have improved a lot with the introduction
of lambdas, method references, and streams (with the possibility of leveraging
the power of the map-filter-reduce triforce); all but the simplest operations
on collections still tend to be quite verbose, painful to achieve, hard to
reason about, difficult to read, refactor and modify. Not to mention the lack
of immutable collections even after so many years!

A few months ago, I embarked on a journey to find a solution to these
annoyances. And after several months of reading, watching videos (I encourage
you to watch [this one](https://youtu.be/QwZF8xQHlxE)), and even toying-around
with code (try these
[katas](https://github.com/eclipse/eclipse-collections-kata)), I've finally
decided on the tool that meets all my needs.

#### The tool

And the unbeatable winner was Eclipse Collections because it...

* Embraces Object Oriented Programming in its APIs, making internal iteration
  the norm. This approach _increase[s] readability by giving names to the
  structural iteration patterns and by reducing code duplication. Moreover, by
  encapsulating implementation within specialized collection types (e.g., list,
  sets, maps), iteration can be optimized for the particular type._
  [[citation](https://github.com/eclipse/eclipse-collections/blob/master/docs/guide.md#-iteration-patterns)]
* Supports
  [lazy](https://github.com/eclipse/eclipse-collections/blob/master/docs/guide.md#-lazy-iteration)
  and
  [parallel-lazy](https://github.com/eclipse/eclipse-collections/blob/master/docs/guide.md#-parallel-lazy-iteration)
  iterations, as well as [protective
  wrappers](https://github.com/eclipse/eclipse-collections/blob/master/docs/guide.md#-protective-wrappers)
  like unmodifiable and synchronized.
* Supplies Bags (replace `Map<T, Integer>` for counting occurrences), MultiMaps
  (replace `Map<T, Collection<R>>` for associating multiple values to a single
  key), BiMaps (maps that allows users to look up from both directions), and
  better-done Stacks, on top of replacements for those collections already
  provided by the Java Collections Framework (JCF).
* Makes the use memory-efficient collection possible by implementing [primitive
  ones](https://github.com/eclipse/eclipse-collections/blob/master/docs/guide.md#-primitive-collections).
* Provides **real** [immutable
  versions](https://github.com/eclipse/eclipse-collections/blob/master/docs/guide.md#-immutable-collections)
  of every single collection it supplies.
* Exposes easy-to-use [container-factory
  classes](https://github.com/eclipse/eclipse-collections/blob/master/docs/guide.md#-creating-mutable-collections)
  for creating new collections (rather than directly calling constructors).
* Facilitates interoperability with JCF's collections with a set of utility
  classes (all of them ending in the word _Iterate_).
* Helps us keeping our APIs' interfaces intact with drop-in replacements for
  JCF's List, Set, and Map (if we're being good and we're using interfaces
  instead of concrete types).
* Enables IDEs to make smart suggestion thanks to its fluent APIs.

---

Eclipse Collections is an incredible library that provides us with a wonderful,
powerful, and fluent API to manage a magnificent set of useful, well-thought
and -implemented collections.

---


### [Allure Framework](http://allure.qatools.ru/)

#### The story

I started my career as a _full-stack_ developer with a strong focus on the
backend, and even then, having a way to wrap my head around the quality of the
software crafting I was involved in was invaluable. Nowadays, after more than
three years as a Test Automation Engineer (QA Engineer, QA Automation
Specialist or the exotic name you organization has chosen to call this role),
visualizing the results of running our test suites plus the trends around all
executions over time is not just vital but also my responsibility.

Test reports are used for fulfilling a wide range of needs:

* They are essential to the metrics the product managers must compile and
  deliver to all stakeholders.
* They are the first source of input developers must use in their research when
  something's wrong.
* They state how much of the code have been covered by tests, which modules are
  most likely to break when new features are introduced, what are those regions
  of the system where bugs appear with greater frequency, how flaky our tests
  are, and so much more information that's crucial in any software development
  effort.
* They represent one of the most indispensable assets for internal as well as
  external audit processes.

For all of these reasons, having a tool for generating test reports packed with
meaningful information that's easy to read and navigate-through, is as
important as any other aspect of the software development life cycle.

In Java, while they are a handful of tools to choose from, there's only one
that offers beautiful HTML-based reports that expose detailed information about
our tests alongside amazing charts that summarize them, and that does all of
this for free.

#### The tool

[Allure Framework](http://allure.qatools.ru/) is a multi-platform and
multi-language reporting tool. It can be used with Java, Python, JavaScript,
Ruby, Groovy, PHP, .Net, and Scala. In the JVM world, it can be implemented via
Maven or Gradle. When it comes to test frameworks, Allure integrates seamlessly
with all the big players: JUnit (4 and 5), TestNG, Cucumber JVM, and Selenide.
Therefore, it also supports behavior-driven testing, in addition to the more
traditional unit testing approach.

It is remarkable how easy it's to adopt Allure Framework, especially with our
existing stack conformed by Maven and JUnit5. A few additional lines must be
introduced into the POM file and that's it, in an authentic plug-and-play way.

It's amazing how much information gets packed in the
[reports](https://docs.qameta.io/allure/#_report_structure) Allure produces and
how it presents said information. We can navigate our tests by
[suites](https://docs.qameta.io/allure/#_suites),
[packages](https://docs.qameta.io/allure/#_packages), and
[behaviors](https://docs.qameta.io/allure/#_behaviors) (if we are making use of
concepts like epics, features and stories). We can visualize
[statistics](https://docs.qameta.io/allure/#_graphs), [history
trends](https://docs.qameta.io/allure/#_overview_page), and even contemplate
how the tests were executed in a [beautiful
timeline](https://docs.qameta.io/allure/#_timeline). Plus, enriching and
customizing the reports with
[categories](https://docs.qameta.io/allure/#_categories_2), [environment
information](https://docs.qameta.io/allure/#_environment), and
[attachments](https://docs.qameta.io/allure/#_attachments) couldn't be easier.

---

_Allure Framework is a flexible lightweight multi-language test report tool
that not only shows a very concise representation of what have been tested in a
neat web report form, but allows everyone participating in the development
process to extract maximum of useful information from everyday execution of
tests._ [[citation](https://docs.qameta.io/allure/)]

---


## Closing words

We have reached the end of our journey. We covered a fairly colorful range of
concepts, technologies and tools that (hopefully) will improve the way we
approach new projects, as well as enhance existing ones, making us better
crafters and professionals on the most ever-changing, complex and exciting
adventure there is: software development.

It is fine if the stack you end up choosing looks very different from the one I
proposed to you, there's no silver bullet, there's no a definitive and absolute
truth for every situation, project, group of people or individual. However, I
hope this article has made you think about aspects of the software development
life cycle you've never pondered about before, it has introduced you to tools
you didn't know existed and it got you exited about and, overall, it has given
you a great time while reading, discovering, tinkering, and thinking.

Thank you so much for staying with me and let's hope for this to be repeated in
the near future.

---

_"An investment in knowledge pays the best interest."_

\- Benjamin Franklin

---
