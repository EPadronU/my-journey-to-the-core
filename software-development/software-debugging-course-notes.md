# Udacity's Software Debugging Course [(CS259)](https://www.udacity.com/course/software-debugging--cs259)

---
Notes as I took them in 2014
---

## Lesson 1 - How Debuggers Work

### The devil's guide to debugging

1. Scatter output statements everywhere
2. Debug the program into existence
3. Never back up earlier versions
4. Don't bother understanding what the program should do
5. Use the most obvious fix

### Error, defect, infection and failure

- Error: not correct, not right or not true
- Defect: error in the code (bug) that produce a infection
- Infection: error in the program state
- Failure: error visible by the user

### The Scientific method of debugging

    code failure       ---------- support - refine ----------
      \    /         /                                        \
       -----> Hypotesis --> Prediction --> Experiment --> Observation --> Diagnosis
         \           \                                        /
         runs          --------- reject - create new --------

### After the fix

- Check whether the same error has been made elsewhere
- Ensure that the error does not occur again
    - Tests
    - Assertions

### Explicit debugging

- Write down what you’re doing
- Make notes about what you see
- Make notes about what you expect
- Write down what your current hypothesis is


## Lesson 2 - Assertions

### Assertions

Assertions in the code **are there to stay**

- for catching errors
- for testing
- for documentation

### Preconditions and postconditions

- Precondition: is a group of assertions being called at the beginning of a
  function and which checks the properties of the arguments.
- Postcondition: is a group of assertions being called at the end of a function
  and which checks the result of it.

### Invariant

An invariant is a condition that always (at the beginning and the at end of
each public method) hold for a data object.

### Assertions are optional

- Assertions can be turned off.
- Assertions must not change the program semantics.
- Assertions should not check for public preconditions

### Should assertions remain enabled in production code?

- Pros
    - Failing is better than bad data
    - Eases debugging
    - Defects in the fields are hard to track
- Cons
    - Performance
    - Not user-friendly

### Get started with assertions

1. Define data invariants
2. Provide preconditions
3. Provide postconditions
4. Run a system invariant checker


## Lesson 3 - Simplifying Failures

### What is it that makes something simple?

- Burden to *understand* something.
- Burden to *explain* something.

### What is relevant information?

If it's different, then it changes the behaviour.

### The Tao of simplifying

“The goal of simplifying the test case is to make it so simple that changing
any aspect of it changes the behaviour of the error”. Steve McConnell

“Everything should be made as simple as possible, but no simpler”. (?)

### A simple process for simplifying

- For every *circumstance* of the problem, check whether it is **relevant** for
  the problem to occur.
- If not, remove it!

### Automated simplification

We need:

- A strategy
    - Binary search (Kernighan & Pike):
        1. Throw away **half the input** and see if the output is still wrong.
        2. If not, **go back to the previous state** and discard **the other
           half** of the input.
    - Delta debugging:
        1. Split the input into **n** subsets (initially **n <- 2**)
        2. If after removing any of these subsets the test fails, proceed with
           this subset and **n <- max(n - 1, 2)**
        3. Otherwise, increase the granularity (**n <- min(2n, |input|)**)
- An automated test

### Causes and causality

- Let **A** and **B** be two events, with **A** *preceding* **B**.
- **A** *causes* **B** if **B** had not occurred if **A** had not occurred.

#### Actual cause

An **actual cause** assumes and changes as little as possible, yet changes the
effect.

#### Defect fixing

When we fix a defect, we want to make sure that:

- It is an **error** [show how to correct it].
- It **causes** the failure [show that the failure no longer occurs after the
  correction].


## Lesson 4 - Tracking Origins

### The Art of Deduction

"Something impossible occurred, and the only solid information is that it
**did** occur. So we must think **backwards from the result** to discover the
reasons." Kernighan and Pike. The Practice of Programming".

### Data dependency

If a statement *A* writes a variable which *B* then reads, then *B* is **data
dependent** on *A*.

### Control dependency

If a statement *A* controls whether *B* is executed or not, then *B* is
**control dependent** on *A*.

### Backward slice

A **backward slice** of *S* contains all statements that *S* would
(transitively) depend upon.

### Forward slice

A **forward slice** of *S* contains all statements that (transitively) depend
on *S*.

### Dynamic slices

- Apply to **executions** instead of programs.
- Tells what **has happend** in the execution.

PROGRAM => TRACE => DYNAMIC SLICE

- Dynamic slices form small subsets of programs:
    - Contains only execute lines
    - Contains only statements that influence the output

### Dependencies and the scientific method

- Use dependencies to find **possible origins**
- Use the scientific method to **track down infections**

## Lesson 5 - Reproducing Failures

### Bug category

- Bohr bug
    - Repeatable
    - Manifest reliably under a set of conditions
- Heisen bug
    - Disappears or change when one attempts to probe or isolate it
- Mandel bug
    - Appears chaotic or even non-deterministic
- Schrödin bug
    - Does not manifest until someone realizes it never should have worked

### The many facets of input

- Static data
    - All that is under the user's control (files, tables..).
- User interaction
    - Capture/replay tool.
- Date and time
- Randomness
- Operating environment
- Schedules
- Debugging tools
- Physics


## Lesson 6 - Learning from Mistakes

### What goes in a bug report

- The problem history (steps needed to reproduce the problem)
- Diagnostic information (core dumps, stack traces, logs)
- The experienced behaviour
- The expected behaviour (as a reality check)
- A one-line summary

### Problem database

- Store useful information about the problems or bugs found on a computer
  system.
- It can also be use to store requirements.
- Bugzilla is an popular open source problem database.

### The problem life cycle

                                         /- Invalid -\
                                        /- Duplicate -\
    |Unconfirmed| -> |New| -> |Assigned| ---Fixed ---> |Resolved| - Fixed 
                                  ^     \- Worskforme-/    |          |
                                  |      \- Wontfix -/     |          v
                                  |         _______________/      |Verified|
                                  |        /                          |
                              |Reopened| <- |Closed| <--------------- / 

### Housekeeping - Issues in problem databases

- Duplicates
- Obsolete problems
    - Will never be fixed
    - Old and occurred only once
    - Old and occurred only internally
- Subsuming test cases

### Where do bugs come from?

- Does bug density correlate with developer experience?
    - Yes. The more experience, the higher the bug density.
- Will there be more bugs where many of them were found in the past?
   - Yes.
- Does complexity matter?
    - No.
- Is heavily tested code less buggy?
    - No. Tests can't find all the bugs.
- Does team structure matter?
    - Yes. The higher the "common manager level", the higher the number of bugs
      is produced.
- Does the problem domain matter?
    - Yes. (Changes frequently, not well defined or complex).


## Recap

### The seven steps of debugging

1. Track the problem
2. Reproduce the problem
3. Automate & simplified
4. Find possible infection origins
5. Focus on most likely origins
6. Isolate the infection chain
7. Correct the defect

### How to avoid debugging

- Get your **requirements** right. ("Without specifications, there are no bugs
  only surprises." Brian Kernighan.)
- Increase **precision** and **automation**.
- Reduce **complexity**.
- Set up **assertions**.
- Test **early** & **often**.
- **Review** your code.
- Analyze **problem history**.
- **Learn** from your mistakes.
