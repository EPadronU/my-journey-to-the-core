# Creating a Simple Service with AWS's API Gateway and a Java Lambda

We are going to develop and deploy a simple service that says _hi_ to its
client. In order to accomplish that, we will receive and send JSON messages and
make use of AWS's infrastructure, particularly **API Gateway** and **Lambda**.

## Source code

For the lambda function will be using **Java 11 with Maven**.

### Initializing the project

You can create the project's folder estructure by hand, or you can use Maven's
archetype to speed things up like this:

```bash
mvn archetype:generate -DarchetypeArtifactId=maven-archetype-quickstart
```

---
We have to indicate the project's group ID, artifact ID, version and package as
they're requested by the interactive utility Maven's archetype shows us.

### Project's folder structure

At the end, you should end up with a folder structure similar to this one:

```
aws-java-lambda/
├── pom.xml
└── src
├── main
│   └── java
│       └── com
│           └── gitlab
│               └── epadronu
│                   └── App.java
└── test
    └── java
        └── com
            └── gitlab
                └── epadronu
                    └── AppTest.java
```

### pom.xml

Now, we need to introduce some plugins and dependencies into our `pom.xml` file.

- Dependencies:
    - `aws-lambda-java-core` - Provides classes for creating the request
      handler for our lambda function
    - `aws-lambda-java-events` - Provides classes for handling events, in
      particular the ones we are going to receive from API Gateway
    - `aws-lambda-java-log4j2` - Defines a Log4j appender we'll use for logging
      in CloudWatch
    - `log4j-api` & `log4j-core` - The logging facade and implementation to be
      used in the project
    - `jackson-databind` - Provides a mechanism for POJOS to JSON serialization
      and de-serialization
    - `lombok` - Defines a set of annotations that will reduce boilerplate code
      a lot
    - `junit-jupiter-api` & `junit-jupiter-engine` - For the testing engine
    - `rest-assured` - For testing our service - For testing our service
- Plugins
    - `maven-surefire-plugin` - Required for running the tests
    - `maven-shade-plugin` - Needed in order to package all the project's
      dependencies with it in the same JAR
    - `maven-shade-plugin.log4j2-cachefile-transformer` - Required for making
      use of the Log4j appender provided by AWS

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <!-- Maven Project Coordinates -->
  <groupId>com.gitlab.epadronu</groupId>
  <artifactId>aws-java-lambda</artifactId>
  <version>1.0-SNAPSHOT</version>

  <!-- Project Properties -->
  <properties>
    <!-- Settings -->
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>11</maven.compiler.source>
    <maven.compiler.target>11</maven.compiler.target>

    <!-- Plugin' Versions -->
    <log4j2.cache.file.transformer.version>2.13.1</log4j2.cache.file.transformer.version>
    <maven.shade.plugin.version>3.2.3</maven.shade.plugin.version>
    <maven.surefire.plugin.version>3.0.0-M4</maven.surefire.plugin.version>

    <!-- Dependencies' Versions -->
    <aws.lambda.core.version>1.2.0</aws.lambda.core.version>
    <aws.lambda.events.version>2.2.7</aws.lambda.events.version>
    <aws.lambda.log4j2.version>1.1.0</aws.lambda.log4j2.version>
    <jackson.version>2.10.3</jackson.version>
    <junit.version>5.6.2</junit.version>
    <log4j.version>2.13.1</log4j.version>
    <lombok.version>1.18.12</lombok.version>
    <rest.assured.version>4.3.0</rest.assured.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-bom</artifactId>
        <version>${log4j.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>org.junit</groupId>
        <artifactId>junit-bom</artifactId>
        <version>${junit.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <!-- Compile -->
    <dependency>
      <groupId>com.amazonaws</groupId>
      <artifactId>aws-lambda-java-core</artifactId>
      <version>${aws.lambda.core.version}</version>
    </dependency>
    <dependency>
      <groupId>com.amazonaws</groupId>
      <artifactId>aws-lambda-java-events</artifactId>
      <version>${aws.lambda.events.version}</version>
    </dependency>
    <dependency>
      <groupId>com.amazonaws</groupId>
      <artifactId>aws-lambda-java-log4j2</artifactId>
      <version>${aws.lambda.log4j2.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>${jackson.version}</version>
    </dependency>
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>${lombok.version}</version>
    </dependency>

    <!-- Test -->
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>rest-assured</artifactId>
      <version>${rest.assured.version}</version>
      <scope>test</scope>
    </dependency>

    <!-- Runtime -->
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <scope>runtime</scope>
    </dependency>
  </dependencies>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>${maven.surefire.plugin.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <!-- JUnit5 configuration -->
          <statelessTestsetReporter implementation="org.apache.maven.plugin.surefire.extensions.junit5.JUnit5Xml30StatelessReporter">
            <usePhrasedTestSuiteClassName>true</usePhrasedTestSuiteClassName>
            <usePhrasedTestCaseClassName>true</usePhrasedTestCaseClassName>
            <usePhrasedTestCaseMethodName>true</usePhrasedTestCaseMethodName>
          </statelessTestsetReporter>

          <consoleOutputReporter implementation="org.apache.maven.plugin.surefire.extensions.junit5.JUnit5ConsoleOutputReporter"/>

          <statelessTestsetInfoReporter implementation="org.apache.maven.plugin.surefire.extensions.junit5.JUnit5StatelessTestsetInfoReporter">
            <usePhrasedClassNameInRunning>true</usePhrasedClassNameInRunning>
            <usePhrasedClassNameInTestCaseSummary>true</usePhrasedClassNameInTestCaseSummary>
          </statelessTestsetInfoReporter>
        </configuration>
      </plugin>
      <plugin> <!-- Fat jar -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>${maven.shade.plugin.version}</version>
        <configuration>
          <createDependencyReducedPom>false</createDependencyReducedPom>
        </configuration>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <transformers> <!-- For AWS finding the log4j2.xml file -->
                <transformer
                  implementation="com.github.edwgiz.maven_shade_plugin.log4j2_cache_transformer.PluginsCacheFileTransformer">
                </transformer>
              </transformers>
            </configuration>
          </execution>
        </executions>
        <dependencies>
          <dependency>
            <groupId>com.github.edwgiz</groupId>
            <artifactId>maven-shade-plugin.log4j2-cachefile-transformer</artifactId>
            <version>${log4j2.cache.file.transformer.version}</version>
          </dependency>
        </dependencies>
      </plugin>
    </plugins>
  </build>

  <!-- For maven-shade-plugin.log4j2-cachefile-transformer -->
  <pluginRepositories>
    <pluginRepository>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2/</url>
    </pluginRepository>
  </pluginRepositories>
</project>
```

### Log4j configuration

The following is the configuration will be using for Log4j. This will allow us
to see useful information on AWS's CloudWatch for our lambda function.

```xml
<?xml version="1.0" encoding="UTF-8"?>

<Configuration packages="com.amazonaws.services.lambda.runtime.log4j2">
  <Appenders>
    <Lambda name="Lambda">
      <PatternLayout>
        <pattern>%d{yyyy-MM-dd HH:mm:ss} %X{AWSRequestId} %-5p %c{1} - %m%n</pattern>
      </PatternLayout>
    </Lambda>
  </Appenders>

  <Loggers>
    <Root level="DEBUG">
      <AppenderRef ref="Lambda"/>
    </Root>

    <Logger name="software.amazon.awssdk" level="WARN" />

    <Logger name="software.amazon.awssdk.request" level="DEBUG" />
  </Loggers>
</Configuration>
```
---
Since all the configuration is already set, let's start with the code itself.

### The models

We are going to use two models (transfer objects or whatever you prefer to call
them) in this project:

#### Message

This one will be used for greeting our service's client.

```java
package com.gitlab.epadronu.aws.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Message {

  private String content;
}
```

#### Person

And this one will be used by the client in order to provide our service with a
name.

```java
package com.gitlab.epadronu.aws.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Person {

  private String name;
}
```

### The Greeter request handler

Now it's time to work on the exiting part: the lambda function. Here we process
the events sent by API Gateway when a client communicates with our service.

We are going to handle only two HTTP methods: `GET` and `POST`. In order to do
that, we will create two Java methods: `onGet` and `onPost`.

The `handleRequest` is where we'll determine what HTTP method was used in the
request performed by the client, and call the proper `on*` method in order to
handle such a request.

Finally, we'll make used of `jackson` for JSON serialization and
de-serialization, and `Log4j` for logging useful information we can visualize
in CloudWatch latter.

```java
package com.gitlab.epadronu.aws.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.epadronu.aws.models.Message;
import com.gitlab.epadronu.aws.models.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Greeter
  implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  private static final Logger logger = LogManager.getLogger(Greeter.class);

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final APIGatewayProxyResponseEvent serverErrorResponse =
    new APIGatewayProxyResponseEvent().withStatusCode(500);

  private static final APIGatewayProxyResponseEvent methodNotAllowedResponse =
    new APIGatewayProxyResponseEvent().withStatusCode(405);

  @Override
  public APIGatewayProxyResponseEvent handleRequest(
    final APIGatewayProxyRequestEvent requestEvent, final Context context) {
    logger.debug("The request was: {} ", requestEvent.toString());

    switch (requestEvent.getHttpMethod()) {
      case "POST":
        return onPost(requestEvent);
      case "GET":
        return onGet();
      default:
        return methodNotAllowedResponse;
    }
  }

  private APIGatewayProxyResponseEvent onGet() {
    final var greetings = Message.builder().content("Hello, client").build();

    try {
      return new APIGatewayProxyResponseEvent().withBody(mapper.writeValueAsString(greetings));
    } catch (JsonProcessingException e) {
      logger.atError().withThrowable(e).log("Cannot serialize message");

      return serverErrorResponse;
    }
  }

  private APIGatewayProxyResponseEvent onPost(final APIGatewayProxyRequestEvent requestEvent) {
    final Person person;

    try {
      person = mapper.readValue(requestEvent.getBody(), Person.class);
    } catch (JsonProcessingException e) {
      logger.atError().withThrowable(e).log("Cannot deserialize person");

      return serverErrorResponse;
    }

    final var greetings = Message.builder().content("Hello, " + person.getName()).build();

    try {
      return new APIGatewayProxyResponseEvent().withBody(mapper.writeValueAsString(greetings));
    } catch (JsonProcessingException e) {
      logger.atError().withThrowable(e).log("Cannot serialize message");

      return serverErrorResponse;
    }
  }
}
```

### Final source code tree

You project's folder structure should look like something similar to this:

```
aws-java-lambda/
├── pom.xml
└── src
├── main
│   ├── java
│   │   └── com
│   │       └── gitlab
│   │           └── epadronu
│   │               └── aws
│   │                   ├── lambda
│   │                   │   └── Greeter.java
│   │                   └── models
│   │                       ├── Message.java
│   │                       └── Person.java
│   └── resources
│       └── log4j2.xml
└── test
    └── java
        └── com
            └── gitlab
                └── epadronu
                    └── aws
                        └── lambda
                            └── GreeterTests.java
```

---
**Gotcha!** We can't forget to include some testing. So let's do exactly that.

### Testing the service with Junit5 & rest-assured

```java
package com.gitlab.epadronu.aws.lambda;

import java.util.Objects;

import com.gitlab.epadronu.aws.models.Person;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.is;

public class GreeterTests {

  private static final String serviceUrl =
    Objects.requireNonNull(System.getProperty("SERVICE_URL"), "The service's URL was not provided");

  @Test
  public void whenSendingAGetRequestWeShouldBeGreeted() {
    when()
      .get(serviceUrl)
      .then()
      .statusCode(200)
      .contentType(ContentType.JSON)
      .body("content", is("Hello, client"));
  }

  @Test
  public void whenSendingAPostRequestWeShouldBeGreeted() {
    final String name = "Edinson";

    given()
      .body(Person.builder().name(name).build())
      .when()
      .post(serviceUrl)
      .then()
      .statusCode(200)
      .contentType(ContentType.JSON)
      .body("content", is("Hello, " + name));
  }

  @Test
  public void whenSendingAPutWeShouldGetA405() {
    when()
      .put(serviceUrl)
      .then()
      .statusCode(405)
      .body(is(""));
  }
}
```


## AWS setup

### aws-cli

- Install `aws-cli` in your system following the [oficial
  documentation](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
- Configure your credentials as it is explained in the [oficial
  documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

### The lambda

1. Navigate to **AWS Lambda** in your AWS console
2. Click on _Create function_
    1. Choose a name for your lambda (`java-lambda` for example)
    2. Choose `Java 11` for the _runtime_
3. Change the _handler_ to `com.gitlab.epadronu.aws.lambda.Greeter` (remember to save your changes)
4. Build and package the JAR as follows:
    ```bash
    mvn clean package
    ```
5. Upload the JAR like this: _(assuming you already have aws-cli installed and
   configured with your credentials)_
    ```bash
    aws lambda update-function-code --function-name java-lambda --zip-file fileb://target/aws-java-lambda-1.0-SNAPSHOT.jar
    ```
    _(modify the function name and the path to the JAR accordingly)_

### The endpoint

1. Click on _Add trigger_
2. Select _API Gateway_
3. Choose `REST API` for the _API type_
3. Choose `Open` for the _Security_
4. Click on _Add_
5. Copy the **API endpoint** URL

## Test it

Now you can call your endpoint like this:
```bash
curl '<API endpoint URL>' --compressed -H 'Accept: application/json;charset=UTF-8'
```

Or this:
```bash
curl '<endpoint-url>' --compressed \
    -H 'Content-Type: application/json;charset=UTF-8' \
    -H 'Accept: application/json;charset=UTF-8' \
    --data-raw '{"name":"Your name"}'
```

Or better yet, run the tests:

```bash
mvn -DSERVICE_URL='<endpoint-url>' clean verify
```

## Logs

Finally, here we can see the CloudWatch logs for the requests send by the
tests:

```log
START RequestId: fce5d688-04a5-41fd-980a-4f01567f7fdd Version: $LATEST
2020-04-24 13:32:16 fce5d688-04a5-41fd-980a-4f01567f7fdd DEBUG Greeter - The request was: {resource: /java-lambda,path: /java-lambda,httpMethod: GET,headers: {Accept=*/*, Accept-Encoding=gzip,deflate, Host=xxxxxxxxx.execute-api.us-east-1.amazonaws.com, User-Agent=Apache-HttpClient/4.5.3 (Java/11.0.7), X-Amzn-Trace-Id=Root=1-5ea2ea60-5a65ef2c8093c6d87fe56198, X-Forwarded-For=xxx.xxx.xxx.xxx, X-Forwarded-Port=443, X-Forwarded-Proto=https},multiValueHeaders: {Accept=[*/*], Accept-Encoding=[gzip,deflate], Host=[xxxxxxxxx.execute-api.us-east-1.amazonaws.com], User-Agent=[Apache-HttpClient/4.5.3 (Java/11.0.7)], X-Amzn-Trace-Id=[Root=1-5ea2ea60-5a65ef2c8093c6d87fe56198], X-Forwarded-For=[xxx.xxx.xxx.xxx], X-Forwarded-Port=[443], X-Forwarded-Proto=[https]},requestContext: {accountId: xxxxxxxxxxxxxxx,resourceId: niocy5,stage: default,requestId: ffa423f3-c4e6-4e69-9620-417e3d55fc20,identity: {sourceIp: xxx.xxx.xxx.xxx,userAgent: Apache-HttpClient/4.5.3 (Java/11.0.7),},resourcePath: /java-lambda,httpMethod: GET,apiId: xxxxxxxxx,path: /default/java-lambda,},isBase64Encoded: false} 
END RequestId: fce5d688-04a5-41fd-980a-4f01567f7fdd
REPORT RequestId: fce5d688-04a5-41fd-980a-4f01567f7fdd	Duration: 3.48 ms	Billed Duration: 100 ms	Memory Size: 512 MB	Max Memory Used: 124 MB	

START RequestId: 3308873a-a440-4819-9ec3-24316f2c3f5c Version: $LATEST
2020-04-24 13:32:18 3308873a-a440-4819-9ec3-24316f2c3f5c DEBUG Greeter - The request was: {resource: /java-lambda,path: /java-lambda,httpMethod: PUT,headers: {Accept=*/*, Accept-Encoding=gzip,deflate, Host=xxxxxxxxx.execute-api.us-east-1.amazonaws.com, User-Agent=Apache-HttpClient/4.5.3 (Java/11.0.7), X-Amzn-Trace-Id=Root=1-5ea2ea62-0c07cf24e5e8da6cd587a824, X-Forwarded-For=xxx.xxx.xxx.xxx, X-Forwarded-Port=443, X-Forwarded-Proto=https},multiValueHeaders: {Accept=[*/*], Accept-Encoding=[gzip,deflate], Host=[xxxxxxxxx.execute-api.us-east-1.amazonaws.com], User-Agent=[Apache-HttpClient/4.5.3 (Java/11.0.7)], X-Amzn-Trace-Id=[Root=1-5ea2ea62-0c07cf24e5e8da6cd587a824], X-Forwarded-For=[xxx.xxx.xxx.xxx], X-Forwarded-Port=[443], X-Forwarded-Proto=[https]},requestContext: {accountId: xxxxxxxxxxxxxxx,resourceId: niocy5,stage: default,requestId: c805bb65-816d-4a55-bfa4-61d6ff19b763,identity: {sourceIp: xxx.xxx.xxx.xxx,userAgent: Apache-HttpClient/4.5.3 (Java/11.0.7),},resourcePath: /java-lambda,httpMethod: PUT,apiId: xxxxxxxxx,path: /default/java-lambda,},isBase64Encoded: false} 
END RequestId: 3308873a-a440-4819-9ec3-24316f2c3f5c
REPORT RequestId: 3308873a-a440-4819-9ec3-24316f2c3f5c	Duration: 3.07 ms	Billed Duration: 100 ms	Memory Size: 512 MB	Max Memory Used: 124 MB	

START RequestId: 7abc0092-51c0-427c-bc55-6df2fd0ba3fb Version: $LATEST
2020-04-24 13:32:18 7abc0092-51c0-427c-bc55-6df2fd0ba3fb DEBUG Greeter - The request was: {resource: /java-lambda,path: /java-lambda,httpMethod: POST,headers: {Accept=*/*, Accept-Encoding=gzip,deflate, Content-Type=text/plain; charset=ISO-8859-1, Host=xxxxxxxxx.execute-api.us-east-1.amazonaws.com, User-Agent=Apache-HttpClient/4.5.3 (Java/11.0.7), X-Amzn-Trace-Id=Root=1-5ea2ea62-02ddfc00912093f0f1502ec0, X-Forwarded-For=xxx.xxx.xxx.xxx, X-Forwarded-Port=443, X-Forwarded-Proto=https},multiValueHeaders: {Accept=[*/*], Accept-Encoding=[gzip,deflate], Content-Type=[text/plain; charset=ISO-8859-1], Host=[xxxxxxxxx.execute-api.us-east-1.amazonaws.com], User-Agent=[Apache-HttpClient/4.5.3 (Java/11.0.7)], X-Amzn-Trace-Id=[Root=1-5ea2ea62-02ddfc00912093f0f1502ec0], X-Forwarded-For=[xxx.xxx.xxx.xxx], X-Forwarded-Port=[443], X-Forwarded-Proto=[https]},requestContext: {accountId: xxxxxxxxxxxxxxx,resourceId: niocy5,stage: default,requestId: 84f7678b-bd02-4097-bea6-eec7fb1681ab,identity: {sourceIp: xxx.xxx.xxx.xxx,userAgent: Apache-HttpClient/4.5.3 (Java/11.0.7),},resourcePath: /java-lambda,httpMethod: POST,apiId: xxxxxxxxx,path: /default/java-lambda,},body: {"name":"Edinson"},isBase64Encoded: false} 
END RequestId: 7abc0092-51c0-427c-bc55-6df2fd0ba3fb
REPORT RequestId: 7abc0092-51c0-427c-bc55-6df2fd0ba3fb	Duration: 191.80 ms	Billed Duration: 200 ms	Memory Size: 512 MB	Max Memory Used: 125 MB	
```

Neat, isn't it?

## Resources

- [AWS Lambda Deployment Package in
  Java](https://docs.aws.amazon.com/lambda/latest/dg/java-package.html)
- [AWS Lambda Function Logging in
  Java](https://docs.aws.amazon.com/lambda/latest/dg/java-logging.html)
